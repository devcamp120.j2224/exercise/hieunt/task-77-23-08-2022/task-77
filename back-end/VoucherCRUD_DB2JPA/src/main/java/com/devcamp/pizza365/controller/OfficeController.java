package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;
import com.devcamp.pizza365.service.OfficeService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OfficeController {
    @Autowired
    IOfficeRepository iOfficeRepository;

	@Autowired
	OfficeService officeService;

	@GetMapping("/offices")
	public List<Office> getAllOffice(){
		return officeService.getAllOffices();
	}
	
	@GetMapping("/offices/{id}")
	public ResponseEntity<Object> getOfficeById(@PathVariable Long id) {
		Office existedOffice = officeService.getOfficeById(id);
		if(existedOffice != null)
			return ResponseEntity.ok(existedOffice);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/offices")
	public ResponseEntity<Object> createOffice(@RequestBody Office pOffice) {
		Office newOffice = officeService.createOffice(pOffice);
		try {
			Office savedOffice = iOfficeRepository.save(newOffice);
			return new ResponseEntity<>(savedOffice, HttpStatus.CREATED);
		} catch(Exception ex) {
			return ResponseEntity.unprocessableEntity()
				.body(ex.getCause().getMessage());
		}
	}
	
	@PutMapping("/offices/{id}")
	public ResponseEntity<Object> updateOfficeById(
		@PathVariable Long id, @RequestBody Office pOffice) {
		Office updatedOffice = officeService.updateOfficeById(id, pOffice);
		if(updatedOffice != null) {
			try {
				Office savedOffice = iOfficeRepository.save(updatedOffice);
				return ResponseEntity.ok(savedOffice);
			} catch(Exception ex) {
				return ResponseEntity.unprocessableEntity()
					.body(ex.getCause().getMessage());
			}
		} else
			return new ResponseEntity<>( HttpStatus.NOT_FOUND);
	}
	@Transactional
	@DeleteMapping("/offices/{id}")  
	public ResponseEntity<Object> deleteOfficeById(@PathVariable Long id){
		return officeService.deleteOfficeById(id);
	}

    // @GetMapping("/offices")
	// public ResponseEntity<Object> getAllOffice() {
	// 	try {
	// 		 return new ResponseEntity<>(iOfficeRepository.findAll(), HttpStatus.OK);
	// 	} catch (Exception e) {
	// 		return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
		
	// }
	
	// @GetMapping("/office/details/{id}")
	// public Object getOfficeById(@PathVariable Long id) {
	// 	if (iOfficeRepository.findById(id).isPresent())
	// 		return iOfficeRepository.findById(id).get();
	// 	else
	// 		return "Can't find special office: " + id;
	// }
	
	// /**
	//  * @param cOffice
	//  * @return
	//  */
	// @PostMapping("/office/create")
	// public ResponseEntity<Object> createOffice(@RequestBody Office cOffice) {
	// 	try {
	// 		Office newOffice = new Office();
	// 		newOffice.setAddressLine(cOffice.getAddressLine());
	// 		newOffice.setCity(cOffice.getCity());
	// 		newOffice.setCountry(cOffice.getCountry());
	// 		newOffice.setPhone(cOffice.getPhone());
	// 		newOffice.setState(cOffice.getState());
	// 		newOffice.setTerritory(cOffice.getTerritory());
			
	// 		Office savedOffice = iOfficeRepository.save(cOffice);
	// 		return new ResponseEntity<>(savedOffice, HttpStatus.CREATED);
	// 	} catch (Exception e) {
	// 		//return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 		return ResponseEntity.unprocessableEntity().body("Failed to Create specified Office: "+e.getCause().getCause().getMessage());
	// 	}
	// }
	
	// /**
	//  * @param id
	//  * @param cOffice
	//  * @return
	//  */
	// @PutMapping("/office/update/{id}")
	// public ResponseEntity<Object> updateOffice(@PathVariable("id") Long id, @RequestBody Office cOffice) {
	// 	Optional<Office> officeData = iOfficeRepository.findById(id);
	// 	if (officeData.isPresent()) {
	// 		Office newOffice = officeData.get();
			
	// 		newOffice.setAddressLine(cOffice.getAddressLine());
	// 		newOffice.setCity(cOffice.getCity());
	// 		newOffice.setCountry(cOffice.getCountry());
	// 		newOffice.setPhone(cOffice.getPhone());
	// 		newOffice.setState(cOffice.getState());
	// 		newOffice.setTerritory(cOffice.getTerritory());
			
	// 		Office savedOffice = iOfficeRepository.save(newOffice);
	// 		return new ResponseEntity<>(savedOffice, HttpStatus.OK);
	// 	} else {
	// 		return ResponseEntity.unprocessableEntity().body("Failed to Update specified Office: " + id);
	// 	}
	// }
	
	
	// @DeleteMapping("/office/delete/{id}")
	// public ResponseEntity<Object> deleteOfficeById(@PathVariable Long id) {
	// 	try {
	// 		iOfficeRepository.deleteById(id);
	// 		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// 	} catch (Exception e) {
	// 		System.out.println("=================="+e.getCause().getCause().getMessage());
	// 		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
	// }
}
