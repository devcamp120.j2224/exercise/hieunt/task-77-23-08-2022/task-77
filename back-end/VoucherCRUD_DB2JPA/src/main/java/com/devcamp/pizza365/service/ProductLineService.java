package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;

@Service
public class ProductLineService {
    @Autowired
    private IProductLineRepository iProductLineRepository;


     public List<ProductLine> getAllProductLines() {
        List<ProductLine> listProductLine = new ArrayList<ProductLine>();
        iProductLineRepository.findAll().forEach(listProductLine::add);
        return listProductLine;
    }

    public ProductLine getProductLineById(Long id) {
        Optional<ProductLine> existedProductLine = iProductLineRepository.findById(id);
        if(existedProductLine.isPresent()) {
            return existedProductLine.get();
        } else {
            return null;
        }
    }

    public ProductLine createProductLine(ProductLine pProductLine) {
        ProductLine newProductLine = new ProductLine();
        newProductLine.setDescription(pProductLine.getDescription());
        newProductLine.setProductLine(pProductLine.getProductLine());
        // newProductLine.setProducts(pProductLine.getProducts());
        return newProductLine;
    }

    public ProductLine updateProductLineById(Long id, ProductLine pProductLine) {
        Optional<ProductLine> existedProductLine = iProductLineRepository.findById(id);
        if(existedProductLine.isPresent()) {
            ProductLine updatedProductLine = existedProductLine.get();
            updatedProductLine.setDescription(pProductLine.getDescription());
            updatedProductLine.setProductLine(pProductLine.getProductLine());
            return updatedProductLine;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deleteProductLineById(Long id) {
        try {
            iProductLineRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }

    public ResponseEntity<Object> deleteProductLines() {
        try {
            iProductLineRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
