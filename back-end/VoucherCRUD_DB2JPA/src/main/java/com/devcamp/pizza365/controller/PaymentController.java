package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.IPaymentRepository;
import com.devcamp.pizza365.service.PaymentService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class PaymentController {
    
    @Autowired
    private PaymentService paymentService;

    @Autowired
    IPaymentRepository iPaymentRepository;

    @GetMapping("payments")
    public List<Payment> getAllPayments() {
        return paymentService.getAllPayments();
    }


    
    @GetMapping("/payments/customerId/{customerId}")
    public ResponseEntity<Object> getPaymentsByCustomerId(@PathVariable Long customerId) {
        List<Payment> listPayment = paymentService.getPaymentsByCustomerId(customerId);
        if(listPayment != null) {
            return ResponseEntity.ok(listPayment);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/payments/{id}")
    public ResponseEntity<Object> getPaymentById(@PathVariable Long id) {
        Payment existedPayment = paymentService.getPaymentById(id);
        if(existedPayment != null) {
            return ResponseEntity.ok(existedPayment);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/payments/customerId/{customerId}")
    public ResponseEntity<Object> createPayment(
        @PathVariable Long customerId, @RequestBody Payment pPayment) {
        Payment newPayment = paymentService.createPayment(customerId, pPayment);
        if(newPayment != null) {
            try {
                Payment savedPayment = iPaymentRepository.save(newPayment);
                return new ResponseEntity<>(savedPayment, HttpStatus.CREATED);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
                    .body(ex.getCause().getMessage());
            }
        } else 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/payments/{id}")
    public ResponseEntity<Object> updatePaymentById(
        @PathVariable Long id, @RequestBody Payment pPayment) {
        Payment updatedPayment = paymentService.updatePaymentById(id, pPayment);
        if(updatedPayment != null) {
            try {
                Payment savedPayment = iPaymentRepository.save(updatedPayment);
                return ResponseEntity.ok(savedPayment);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
                    .body(ex.getCause().getMessage());
            }
        } else 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Transactional
    @DeleteMapping("/payments/{id}")  
	public ResponseEntity<Object> deletePaymentById(@PathVariable Long id){
		return paymentService.deletePaymentById(id);
    }
}
