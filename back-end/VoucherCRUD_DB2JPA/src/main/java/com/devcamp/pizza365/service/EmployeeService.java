package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    IEmployeeRepository iEmployeeRepository;

    public List<Employee> getAllEmployees() {
        List<Employee> listEmployee = new ArrayList<Employee>();
        iEmployeeRepository.findAll().forEach(listEmployee::add);
        return listEmployee;
    }

    public Employee getEmployeeById(Long id) {
        Optional<Employee> existedEmployee = iEmployeeRepository.findById(id);
        if(existedEmployee.isPresent()) {
            return existedEmployee.get();
        } else {
            return null;
        }
    }

    public Employee createEmployee(Employee pEmployee) {
        Employee newEmployee = new Employee();
        newEmployee.setEmail(pEmployee.getEmail());
        newEmployee.setExtension(pEmployee.getExtension());
        newEmployee.setFirstName(pEmployee.getFirstName());
        newEmployee.setJobTitle(pEmployee.getJobTitle());
        newEmployee.setLastName(pEmployee.getLastName());
        newEmployee.setOfficeCode(pEmployee.getOfficeCode());
        newEmployee.setReportTo(pEmployee.getReportTo());
        return newEmployee;
    }

    public Employee updateEmployeeById(Long id, Employee pEmployee) {
        Optional<Employee> existedEmployee = iEmployeeRepository.findById(id);
        if(existedEmployee.isPresent()) {
            Employee updatedEmployee = existedEmployee.get();
            updatedEmployee.setEmail(pEmployee.getEmail());
            updatedEmployee.setExtension(pEmployee.getExtension());
            updatedEmployee.setFirstName(pEmployee.getFirstName());
            updatedEmployee.setJobTitle(pEmployee.getJobTitle());
            updatedEmployee.setLastName(pEmployee.getLastName());
            updatedEmployee.setOfficeCode(pEmployee.getOfficeCode());
            updatedEmployee.setReportTo(pEmployee.getReportTo());
            return updatedEmployee;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deleteEmployeeById(Long id) {
        try {
            iEmployeeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }

    public ResponseEntity<Object> deleteEmployees() {
        try {
            iEmployeeRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }
}
