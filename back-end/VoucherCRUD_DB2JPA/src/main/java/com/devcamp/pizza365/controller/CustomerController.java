package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.service.CustomerService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {

	@Autowired
	ICustomerRepository customerRepository;

	@Autowired
	private CustomerService customerService;

	// Sử dụng Service

    @GetMapping("customers")
    public ArrayList<Customer> getAllCustomer(){
        return customerService.getListCustomer();
    }

	@GetMapping("/customers/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable Long id) {
        Customer existedCustomer = customerService.getCustomerById(id);
        if(existedCustomer != null) {
            return ResponseEntity.ok(existedCustomer);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

	@PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@RequestBody Customer pCustomer) {
        Customer newCustomer = customerService.createCustomer(pCustomer);
        try {
			Customer savedCustomer = customerRepository.save(newCustomer);
            return new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);
        } catch(Exception ex) {
            return ResponseEntity.unprocessableEntity()
				.body(ex.getCause().getMessage());
        }
    }

	@PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomerById(
		@PathVariable Long id, @RequestBody Customer pCustomer) {
        Customer updatedCustomer = customerService.updateCustomerById(id, pCustomer);
        if(updatedCustomer != null) {
            try {
				Customer savedCustomer = customerRepository.save(updatedCustomer);
                return ResponseEntity.ok(savedCustomer);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
					.body(ex.getCause().getMessage());
            }
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	@Transactional
	@DeleteMapping("/customers/{id}")  
	public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
		return customerService.deleteCustomerById(id);  
    }

	////////////////////////////////////////////////////////////
	@GetMapping("/customers/last-name/{lastName}")
	public ResponseEntity<List<Customer>> getCustomersByLastNameLike(@PathVariable String lastName) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByLastNameLike(lastName).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/first-name/{firstName}")
	public ResponseEntity<List<Customer>> getCustomersByFirstNameLike(@PathVariable String firstName) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByFirstNameLike(firstName).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/city/{city}")
	public ResponseEntity<List<Customer>> getCustomersByCityLike(@PathVariable String city,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByCityLike(city, PageRequest.of(page, 2)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/state/{state}")
	public ResponseEntity<List<Customer>> getCustomersByStateLike(@PathVariable String state,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByStateLike(state, PageRequest.of(page, 2)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/country/{country}")
	public ResponseEntity<List<Customer>> getCustomersByCountryLike(@PathVariable String country,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			customerRepository.findByCountryLike(country, PageRequest.of(page, 6)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/customers/update/{country}")
	public ResponseEntity<Object> updateCountry(@PathVariable String country) {
		try {
			int vCustomer = customerRepository.updateCountry(country);
			return new ResponseEntity<>(vCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
