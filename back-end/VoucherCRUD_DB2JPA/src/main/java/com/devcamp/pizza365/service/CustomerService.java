package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private ICustomerRepository iCustomerRepository;

    /**
     * @return
     */
    public ArrayList<Customer> getListCustomer() {
        ArrayList<Customer> listCustomer = new ArrayList<>();
        iCustomerRepository.findAll().forEach(listCustomer::add);
        return listCustomer;
    }

    public Customer getCustomerById(Long id) {
        Optional<Customer> existedCustomer = iCustomerRepository.findById(id);
        if(existedCustomer.isPresent()) {
            return existedCustomer.get();
        } else {
            return null;
        }
    }

    public Customer createCustomer(Customer pCustomer) {
        Customer newCustomer = new Customer();
        newCustomer.setFirstName(pCustomer.getFirstName());
        newCustomer.setLastName(pCustomer.getLastName());
        newCustomer.setAddress(pCustomer.getAddress());
        newCustomer.setCity(pCustomer.getCity());
        newCustomer.setCountry(pCustomer.getCountry());
        newCustomer.setCreditLimit(pCustomer.getCreditLimit());
        newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
        newCustomer.setPostalCode(pCustomer.getPostalCode());
        newCustomer.setState(pCustomer.getState());
        newCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
        // newCustomer.setOrders(pCustomer.getOrders());
        // newCustomer.setPayments(pCustomer.getPayments());
        return newCustomer;
    }

    
    public Customer updateCustomerById(Long id, Customer pCustomer) {
        Optional<Customer> existedCustomer = iCustomerRepository.findById(id);
        if(existedCustomer.isPresent()) {
            Customer updatedCustomer = existedCustomer.get();
            updatedCustomer.setFirstName(pCustomer.getFirstName());
            updatedCustomer.setLastName(pCustomer.getLastName());
            updatedCustomer.setAddress(pCustomer.getAddress());
            updatedCustomer.setCity(pCustomer.getCity());
            updatedCustomer.setCountry(pCustomer.getCountry());
            updatedCustomer.setCreditLimit(pCustomer.getCreditLimit());
            updatedCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
            updatedCustomer.setPostalCode(pCustomer.getPostalCode());
            updatedCustomer.setState(pCustomer.getState());
            updatedCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
            // updatedCustomer.setOrders(pCustomer.getOrders());
            // updatedCustomer.setPayments(pCustomer.getPayments());
            return updatedCustomer;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deleteCustomerById(Long id) {
        try {
            iCustomerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }

}
