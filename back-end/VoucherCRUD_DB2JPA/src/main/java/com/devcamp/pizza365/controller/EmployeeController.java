package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;
import com.devcamp.pizza365.service.EmployeeService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class EmployeeController {
    @Autowired
    IEmployeeRepository iEmployeeRepository;

	@Autowired
	EmployeeService employeeService;

	@GetMapping("/employees")
	public List<Employee> getAllEmployees(){
		return employeeService.getAllEmployees();
	}

	@GetMapping("/employees/{id}")
	public ResponseEntity<Object> getEmployeeById(@PathVariable Long id) {
		Employee existedEmployee = employeeService.getEmployeeById(id);
		if(existedEmployee != null) {
			return ResponseEntity.ok(existedEmployee);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping("/employees")
	public ResponseEntity<Object> createEmployee(@RequestBody Employee pEmployee) {
		Employee newEmployee = employeeService.createEmployee(pEmployee);
		try {
			Employee savedEmployee = iEmployeeRepository.save(newEmployee);
			return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
		} catch(Exception ex) {
			return ResponseEntity.unprocessableEntity()
				.body(ex.getCause().getMessage());
		}
	}

	@PutMapping("/employees/{id}")
	public ResponseEntity<Object> updateEmployeeById(
		@PathVariable Long id, @RequestBody Employee pEmployee) {
		Employee updatedEmployee = employeeService.updateEmployeeById(id, pEmployee);
		if(updatedEmployee != null) {
			try {
				Employee savedEmployee = iEmployeeRepository.save(updatedEmployee);
				return ResponseEntity.ok(savedEmployee);
			} catch(Exception ex) {
				return ResponseEntity.unprocessableEntity()
					.body(ex.getCause().getMessage());
			}
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	@Transactional
	@DeleteMapping("/employees/{id}")  
	public ResponseEntity<Object> deleteEmployeeById(@PathVariable Long id){
		return employeeService.deleteEmployeeById(id);  
	}

    // @GetMapping("/employees")
	// public ResponseEntity<Object> getAllEmployee() {
	// 	try {
	// 		 return new ResponseEntity<>(iEmployeeRepository.findAll(), HttpStatus.OK);
	// 	} catch (Exception e) {
	// 		return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
		
	// }
	
	// @GetMapping("/employee/details/{id}")
	// public Object getEmployeeById(@PathVariable Long id) {
	// 	if (iEmployeeRepository.findById(id).isPresent())
	// 		return iEmployeeRepository.findById(id).get();
	// 	else
	// 		return "Can't find special employee: " + id;
	// }
	
	// /**
	//  * @param cEmployee
	//  * @return
	//  */
	// @PostMapping("/employee/create")
	// public ResponseEntity<Object> createEmployee(@RequestBody Employee cEmployee) {
	// 	try {
	// 		Employee newEmployee = new Employee();
	// 		newEmployee.setEmail(cEmployee.getEmail());
	// 		newEmployee.setExtension(cEmployee.getExtension());
	// 		newEmployee.setFirstName(cEmployee.getFirstName());
	// 		newEmployee.setJobTitle(cEmployee.getJobTitle());
	// 		newEmployee.setLastName(cEmployee.getLastName());
	// 		newEmployee.setReportTo(cEmployee.getReportTo());
	// 		newEmployee.setOfficeCode(cEmployee.getOfficeCode());
			
	// 		Employee savedEmployee = iEmployeeRepository.save(cEmployee);
	// 		return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
	// 	} catch (Exception e) {
	// 		//return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 		return ResponseEntity.unprocessableEntity().body("Failed to Create specified Employee: "+e.getCause().getCause().getMessage());
	// 	}
	// }
	
	// /**
	//  * @param id
	//  * @param cEmployee
	//  * @return
	//  */
	// @PutMapping("/employee/update/{id}")
	// public ResponseEntity<Object> updateEmployee(@PathVariable("id") Long id, @RequestBody Employee cEmployee) {
	// 	Optional<Employee> employeeData = iEmployeeRepository.findById(id);
	// 	if (employeeData.isPresent()) {
	// 		Employee newEmployee = employeeData.get();
			
	// 		newEmployee.setEmail(cEmployee.getEmail());
	// 		newEmployee.setExtension(cEmployee.getExtension());
	// 		newEmployee.setFirstName(cEmployee.getFirstName());
	// 		newEmployee.setJobTitle(cEmployee.getJobTitle());
	// 		newEmployee.setLastName(cEmployee.getLastName());
	// 		newEmployee.setReportTo(cEmployee.getReportTo());
	// 		newEmployee.setOfficeCode(cEmployee.getOfficeCode());
			
	// 		Employee savedEmployee = iEmployeeRepository.save(newEmployee);
	// 		return new ResponseEntity<>(savedEmployee, HttpStatus.OK);
	// 	} else {
	// 		return ResponseEntity.unprocessableEntity().body("Failed to Update specified Employee: " + id);
	// 	}
	// }
	
	
	// @DeleteMapping("/employee/delete/{id}")
	// public ResponseEntity<Object> deleteEmployeeById(@PathVariable Long id) {
	// 	try {
	// 		iEmployeeRepository.deleteById(id);
	// 		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// 	} catch (Exception e) {
	// 		System.out.println("=================="+e.getCause().getCause().getMessage());
	// 		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
	// }
}
