package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.repository.IProductRepository;
import com.devcamp.pizza365.service.ProductService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductController {
    @Autowired
    ProductService productService;

    @Autowired
    IProductRepository iProductRepository;

	@GetMapping("/products")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/products/productLineId/{productLineId}")
    public ResponseEntity<Object> getProductsByProductLineId(@PathVariable Long productLineId) {
        List<Product> listProduct = productService.getProductsByProductLineId(productLineId);
        if(listProduct != null) {
            return ResponseEntity.ok(listProduct);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable Long id) {
        Product existedProduct = productService.getProductById(id);
        if(existedProduct != null) {
            return ResponseEntity.ok(existedProduct);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products/productLineId/{productLineId}")
    public ResponseEntity<Object> createProduct(
		@PathVariable Long productLineId, @RequestBody Product pProduct) {
        Product newProduct = productService.createProduct(productLineId, pProduct);
        if(newProduct != null) {
            try {
				Product savedProduct = iProductRepository.save(newProduct);
                return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
					.body(ex.getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProductById(
		@PathVariable Long id, @RequestBody Product pProduct) {
        Product updatedProduct = productService.updateProductById(id, pProduct);
        if(updatedProduct != null) {
            try {
				Product savedProduct = iProductRepository.save(updatedProduct);
                return ResponseEntity.ok(savedProduct);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
					.body(ex.getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
	@Transactional
    @DeleteMapping("/products/{id}")  
	public ResponseEntity<Object> deleteProductById(@PathVariable Long id){
		return productService.deleteProductById(id);
    }


    // @GetMapping("/products")
	// public List<Product> getAllProducts() {
	// 	return iProductRepository.findAll();
	// }

	// @GetMapping("/product/details/{id}")
	// public Product getProductById(@PathVariable Long id) {
	// 	if (iProductRepository.findById(id).isPresent())
	// 		return iProductRepository.findById(id).get();
	// 	else
	// 		return null;
	// }

	// @DeleteMapping("/product/delete/{id}")
	// public ResponseEntity<Object> deleteProductById(@PathVariable Long id) {
	// 	try {
	// 		iProductRepository.deleteById(id);
	// 		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// 	} catch (Exception e) {
	// 		System.out.println(e);
	// 		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
	// }

	// @PutMapping("/product/update/{id}")
	// public ResponseEntity<Object> updateProduct(@PathVariable("id") Long id, @RequestBody Product cProduct) {
	// 	Optional<Product> productData = iProductRepository.findById(id);
	// 	if (productData.isPresent()) {
	// 		Product newProduct = productData.get();
	// 		newProduct.setProductCode(cProduct.getProductCode());
	// 		newProduct.setProductName(cProduct.getProductName());
	// 		newProduct.setProductDescripttion(cProduct.getProductDescripttion());
	// 		newProduct.setProductScale(cProduct.getProductScale());
	// 		newProduct.setProductVendor(cProduct.getProductVendor());
	// 		newProduct.setQuantityInStock(cProduct.getQuantityInStock());
	// 		newProduct.setBuyPrice(cProduct.getBuyPrice());
	// 		newProduct.setProductDetails(cProduct.getProductDetails());
		
	// 		Product saveProduct = iProductRepository.save(newProduct);
	// 		return new ResponseEntity<>(saveProduct, HttpStatus.OK);
	// 	} else {
	// 		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// 	}
	// }

	// @GetMapping("/product/byproductlineid/{id}")
	// public List<Product> getProductByProductLineId(@PathVariable("id") Long id) {
	// 	Optional<ProductLine> productLineData = iProductLineRepository.findById(id);
	// 	if (productLineData.isPresent()) {
	// 		return productLineData.get().getProducts();
	// 	} else {
	// 		return null;
	// 	}
	// }

	// @PostMapping("/product/create/{id}")
	// public ResponseEntity<Object> createProduct(@PathVariable("id") Long id, @RequestBody Product cProduct) {
	// 	Optional<ProductLine> productLineData = iProductLineRepository.findById(id);
	// 	if (productLineData.isPresent()) {

	// 		Product newProduct = new Product();
	// 		newProduct.setProductCode(cProduct.getProductCode());
	// 		newProduct.setProductName(cProduct.getProductName());
	// 		newProduct.setProductDescripttion(cProduct.getProductDescripttion());
	// 		newProduct.setProductScale(cProduct.getProductScale());
	// 		newProduct.setProductVendor(cProduct.getProductVendor());
	// 		newProduct.setQuantityInStock(cProduct.getQuantityInStock());
	// 		newProduct.setBuyPrice(cProduct.getBuyPrice());
	// 		newProduct.setProductDetails(cProduct.getProductDetails());

	// 		ProductLine _productLine = productLineData.get();
    //         newProduct.setProductLine(_productLine);

	// 		Product saveProduct = iProductRepository.save(newProduct);
	// 		return new ResponseEntity<>(saveProduct, HttpStatus.CREATED);
	// 	} else {
	// 		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// 	}
	// }
}
