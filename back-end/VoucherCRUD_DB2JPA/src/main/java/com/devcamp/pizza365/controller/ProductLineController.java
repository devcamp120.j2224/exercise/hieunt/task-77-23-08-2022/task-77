package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.service.ProductLineService;


@CrossOrigin
@RestController
@RequestMapping("/")
public class ProductLineController {
	@Autowired
	ProductLineService productLineService;

	@Autowired
	IProductLineRepository productLineRepository;

	@GetMapping("/productlines")
	public List<ProductLine> getAllProductLines(){
		return productLineService.getAllProductLines();
	}

	@GetMapping("/productlines/{id}")
	public ResponseEntity<Object> getProductLineById(@PathVariable Long id) {
		ProductLine existedProductLine = productLineService.getProductLineById(id);
		if(existedProductLine != null) {
			return ResponseEntity.ok(existedProductLine);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping("/productlines")
	public ResponseEntity<Object> createProductLine(@RequestBody ProductLine pProductLine) {
		ProductLine newProductLine = productLineService.createProductLine(pProductLine);
		try {
			ProductLine savedProductLine = productLineRepository.save(newProductLine);
			return new ResponseEntity<>(savedProductLine, HttpStatus.CREATED);
		} catch(Exception ex) {
			return ResponseEntity.unprocessableEntity()
				.body(ex.getCause().getMessage());
		}
	}

	@PutMapping("/productlines/{id}")
	public ResponseEntity<Object> updateProductLineById(
		@PathVariable Long id, @RequestBody ProductLine pProductLine) {
		ProductLine updatedProductLine = productLineService.updateProductLineById(id, pProductLine);
		if(updatedProductLine != null) {
			try {
				ProductLine savedProductLine = productLineRepository.save(updatedProductLine);
				return ResponseEntity.ok(savedProductLine);
			} catch(Exception ex) {
				return ResponseEntity.unprocessableEntity()
					.body(ex.getCause().getMessage());
			}
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	@Transactional
	@DeleteMapping("/productlines/{id}")  
	public ResponseEntity<Object> deleteProductLineById(@PathVariable Long id){
		return productLineService.deleteProductLineById(id);
	}

    // @Autowired
    // IProductLineRepository iProductLineRepository;

    // @GetMapping("/productlines")
	// public ResponseEntity<Object> getAllProductLine() {
	// 	try {
	// 		 return new ResponseEntity<>(iProductLineRepository.findAll(), HttpStatus.OK);
	// 	} catch (Exception e) {
	// 		return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
		
	// }
	
	// @GetMapping("/productline/details/{id}")
	// public Object getProductLineById(@PathVariable Long id) {
	// 	if (iProductLineRepository.findById(id).isPresent())
	// 		return iProductLineRepository.findById(id).get();
	// 	else
	// 		return "Can't find special productline: " + id;
	// }
	
	// /**
	//  * @param cProductLine
	//  * @return
	//  */
	// @PostMapping("/productline/create")
	// public ResponseEntity<Object> createProductLine(@RequestBody ProductLine cProductLine) {
	// 	try {
	// 		ProductLine newProductLine = new ProductLine();
	// 		newProductLine.setDescription(cProductLine.getDescription());
	// 		newProductLine.setProductLine(cProductLine.getProductLine());
	// 		newProductLine.setProducts(cProductLine.getProducts());
			
	// 		ProductLine savedProductLine = iProductLineRepository.save(cProductLine);
	// 		return new ResponseEntity<>(savedProductLine, HttpStatus.CREATED);
	// 	} catch (Exception e) {
	// 		//return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 		return ResponseEntity.unprocessableEntity().body("Failed to Create specified ProductLine: "+e.getCause().getCause().getMessage());
	// 	}
	// }
	
	// /**
	//  * @param id
	//  * @param cProductLine
	//  * @return
	//  */
	// @PutMapping("/productline/update/{id}")
	// public ResponseEntity<Object> updateProductLine(@PathVariable("id") Long id, @RequestBody ProductLine cProductLine) {
	// 	Optional<ProductLine> productlineData = iProductLineRepository.findById(id);
	// 	if (productlineData.isPresent()) {
	// 		ProductLine newProductLine = productlineData.get();
			
	// 		newProductLine.setDescription(cProductLine.getDescription());
	// 		newProductLine.setProductLine(cProductLine.getProductLine());
	// 		newProductLine.setProducts(cProductLine.getProducts());
			
	// 		ProductLine savedProductLine = iProductLineRepository.save(newProductLine);
	// 		return new ResponseEntity<>(savedProductLine, HttpStatus.OK);
	// 	} else {
	// 		return ResponseEntity.unprocessableEntity().body("Failed to Update specified ProductLine: " + id);
	// 	}
	// }
	
	
	// @DeleteMapping("/productline/delete/{id}")
	// public ResponseEntity<Object> deleteProductLineById(@PathVariable Long id) {
	// 	try {
	// 		iProductLineRepository.deleteById(id);
	// 		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// 	} catch (Exception e) {
	// 		System.out.println("=================="+e.getCause().getCause().getMessage());
	// 		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
	// }
}
