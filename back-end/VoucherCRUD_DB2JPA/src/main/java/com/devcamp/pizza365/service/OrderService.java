package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.OrderRepository;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ICustomerRepository iCustomerRepository;

    public List<Order> getAllOrders() {
        List<Order> listOrder = new ArrayList<Order>();
        orderRepository.findAll().forEach(listOrder::add);
        return listOrder;
    }

    public List<Order> getOrdersByCustomerId(Long CustomerId) {
        Optional<Customer> existedCustomer = iCustomerRepository.findById(CustomerId);
        if(existedCustomer.isPresent()) {
            List<Order> listOrder = new ArrayList<Order>();
            existedCustomer.get().getOrders().forEach(listOrder::add);
            return listOrder;
        } else {
            return null;
        }
    }

    public Order getOrderById(Long id) {
        Optional<Order> existedOrder = orderRepository.findById(id);
        if(existedOrder.isPresent()) {
            return existedOrder.get();
        } else {
            return null;
        }
    }

    public Order createOrder(Long customerId, Order pOrder) {
        Optional<Customer> existedCustomer = iCustomerRepository.findById(customerId);
        if(existedCustomer.isPresent()) {
            Order newOrder = new Order();
            newOrder.setComments(pOrder.getComments());
            newOrder.setOrderDate(pOrder.getOrderDate());
            newOrder.setRequiredDate(pOrder.getRequiredDate());
            newOrder.setShippedDate(pOrder.getShippedDate());
            newOrder.setStatus(pOrder.getStatus());
            // newOrder.setOrderDetails(pOrder.getOrderDetails());
            newOrder.setCustomer(existedCustomer.get());
            return newOrder;
        } else {
            return null;
        }
    }

    public Order updateOrderById(Long id, Order pOrder) {
        Optional<Order> existedOrder = orderRepository.findById(id);
        if(existedOrder.isPresent()) {
            Order updatedOrder = existedOrder.get();
            updatedOrder.setComments(pOrder.getComments());
            updatedOrder.setOrderDate(pOrder.getOrderDate());
            updatedOrder.setRequiredDate(pOrder.getRequiredDate());
            updatedOrder.setShippedDate(pOrder.getShippedDate());
            updatedOrder.setStatus(pOrder.getStatus());
            return updatedOrder;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deleteOrderById(Long id) {
        try {
            orderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }

}
