package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.Order;

public interface OrderRepository extends JpaRepository <Order, Long> {
    
}
