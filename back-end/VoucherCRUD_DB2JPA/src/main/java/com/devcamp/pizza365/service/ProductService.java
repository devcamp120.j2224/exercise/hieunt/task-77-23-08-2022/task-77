package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.repository.IProductRepository;

@Service
public class ProductService {
    @Autowired
    IProductRepository  iProductRepository;

    @Autowired
    IProductLineRepository iProductLineRepository;

    public List<Product> getAllProducts() {
        List<Product> listProduct = new ArrayList<Product>();
        iProductRepository.findAll().forEach(listProduct::add);
        return listProduct;
    }

    public List<Product> getProductsByProductLineId(Long ProductLineId) {
        Optional<ProductLine> existedProductLine = iProductLineRepository.findById(ProductLineId);
        if(existedProductLine.isPresent()) {
            List<Product> listProduct = new ArrayList<Product>();
            existedProductLine.get().getProducts().forEach(listProduct::add);
            return listProduct;
        } else {
            return null;
        }
    }

    public Product getProductById(Long id) {
        Optional<Product> existedProduct = iProductRepository.findById(id);
        if(existedProduct.isPresent()) {
            return existedProduct.get();
        } else {
            return null;
        }
    }

    public Product createProduct(Long productLineId, Product pProduct) {
        Optional<ProductLine> existedProductLine = iProductLineRepository.findById(productLineId);
        if(existedProductLine.isPresent()) {
            Product newProduct = new Product();
            newProduct.setBuyPrice(pProduct.getBuyPrice());
            newProduct.setProductCode(pProduct.getProductCode());
            newProduct.setProductDescripttion(pProduct.getProductDescripttion());
            newProduct.setProductName(pProduct.getProductName());
            newProduct.setProductScale(pProduct.getProductScale());
            newProduct.setProductVendor(pProduct.getProductVendor());
            newProduct.setQuantityInStock(pProduct.getQuantityInStock());
            // newProduct.setOrderDetails(pProduct.getOrderDetails());
            newProduct.setProductLine(existedProductLine.get());
            return newProduct;
        } else {
            return null;
        }
    }

    public Product updateProductById(Long id, Product pProduct) {
        Optional<Product> existedProduct = iProductRepository.findById(id);
        if(existedProduct.isPresent()) {
            Product updatedProduct = existedProduct.get();
            updatedProduct.setBuyPrice(pProduct.getBuyPrice());
            updatedProduct.setProductCode(pProduct.getProductCode());
            updatedProduct.setProductDescripttion(pProduct.getProductDescripttion());
            updatedProduct.setProductName(pProduct.getProductName());
            updatedProduct.setProductScale(pProduct.getProductScale());
            updatedProduct.setProductVendor(pProduct.getProductVendor());
            updatedProduct.setQuantityInStock(pProduct.getQuantityInStock());
            return updatedProduct;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deleteProductById(Long id) {
        try {
            iProductRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }

    public ResponseEntity<Object> deleteProducts() {
        try {
            iProductRepository.deleteAll();
            ;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
