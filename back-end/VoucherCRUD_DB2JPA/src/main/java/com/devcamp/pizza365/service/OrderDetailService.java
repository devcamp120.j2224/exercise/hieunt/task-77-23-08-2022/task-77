package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.IProductRepository;
import com.devcamp.pizza365.repository.OrderDetailRepository;
import com.devcamp.pizza365.repository.OrderRepository;

@Service
public class OrderDetailService {
    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    IProductRepository iProductRepository;

    public List<OrderDetail> getAllOrderDetails() {
        List<OrderDetail> listOrderDetail = new ArrayList<OrderDetail>();
        orderDetailRepository.findAll().forEach(listOrderDetail::add);
        return listOrderDetail;
    }

    public List<OrderDetail> getOrderDetailsByOrderId(Long OrderId) {
        Optional<Order> existedOrder = orderRepository.findById(OrderId);
        if(existedOrder.isPresent()) {
            List<OrderDetail> listOrderDetail = new ArrayList<OrderDetail>();
            existedOrder.get().getOrderDetails().forEach(listOrderDetail::add);
            return listOrderDetail;
        } else {
            return null;
        }
    }
    public List<OrderDetail> getOrderDetailsByProductId(Long productId) {
        Optional<Product> existedProduct = iProductRepository.findById(productId);
        if(existedProduct.isPresent()) {
            List<OrderDetail> listOrderDetail = new ArrayList<OrderDetail>();
            existedProduct.get().getOrderDetails().forEach(listOrderDetail::add);
            return listOrderDetail;
        } else {
            return null;
        }
    }

    public OrderDetail getOrderDetailById(Long id) {
        Optional<OrderDetail> existedOrderDetail = orderDetailRepository.findById(id);
        if(existedOrderDetail.isPresent()) {
            return existedOrderDetail.get();
        } else {
            return null;
        }
    }

    public OrderDetail createOrderDetail(Long orderId, Long productId, OrderDetail pOrderDetail) {
        Optional<Order> existedOrder = orderRepository.findById(orderId);
        Optional<Product> existedProduct = iProductRepository.findById(productId);
        if(existedOrder.isPresent() && existedProduct.isPresent()) {
            OrderDetail newOrderDetail = new OrderDetail();
            newOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
            newOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
            newOrderDetail.setProduct(existedProduct.get());
            newOrderDetail.setOrder(existedOrder.get());
            return newOrderDetail;
        } else {
            return null;
        }
    }

    public OrderDetail updateOrderDetailById(Long id, Long productId, OrderDetail pOrderDetail) {
        Optional<OrderDetail> existedOrderDetail = orderDetailRepository.findById(id);
        Optional<Product> existedProduct = iProductRepository.findById(productId);
        if(existedOrderDetail.isPresent() && existedProduct.isPresent()) {
            OrderDetail updatedOrderDetail = existedOrderDetail.get();
            updatedOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
            updatedOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
            updatedOrderDetail.setProduct(existedProduct.get());
            return updatedOrderDetail;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deleteOrderDetailById(Long id) {
        try {
            orderDetailRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
            .body(e.getCause().getMessage());
        }
    }

    public ResponseEntity<Object> deleteOrderDetails() {
        try {
            orderDetailRepository.deleteAll();
            ;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
