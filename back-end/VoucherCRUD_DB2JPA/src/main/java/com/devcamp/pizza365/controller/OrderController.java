package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.OrderRepository;
import com.devcamp.pizza365.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/orders")
    public List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @GetMapping("/orders/customerId/{customerId}")
    public ResponseEntity<Object> getOrdersByCustomerId(@PathVariable Long customerId) {
        List<Order> listOrder = orderService.getOrdersByCustomerId(customerId);
        if(listOrder != null) {
            return ResponseEntity.ok(listOrder);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable Long id) {
        Order existedOrder = orderService.getOrderById(id);
        if(existedOrder != null) {
            return ResponseEntity.ok(existedOrder);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/orders/customerId/{customerId}")
    public ResponseEntity<Object> createOrder(
        @PathVariable Long customerId, @RequestBody Order pOrder) {
        Order newOrder = orderService.createOrder(customerId, pOrder);
        if(newOrder != null) {
            try {
                Order savedOrder = orderRepository.save(newOrder);
                return new ResponseEntity<>(savedOrder, HttpStatus.CREATED);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
                    .body(ex.getCause().getMessage());
            }
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrderById(
        @PathVariable Long id, @RequestBody Order pOrder) {
        Order updatedOrder = orderService.updateOrderById(id, pOrder);
        if(updatedOrder != null) {
            try {
                Order savedOrder = orderRepository.save(updatedOrder);
                return ResponseEntity.ok(savedOrder);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
                    .body(ex.getCause().getMessage());
            }
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @Transactional
    @DeleteMapping("/orders/{id}")  
	public ResponseEntity<Object> deleteOrderById(@PathVariable Long id){
        return orderService.deleteOrderById(id);
    }
}
