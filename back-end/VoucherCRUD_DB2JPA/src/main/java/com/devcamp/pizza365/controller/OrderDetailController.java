package com.devcamp.pizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.repository.OrderDetailRepository;
import com.devcamp.pizza365.service.OrderDetailService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderDetailController {
    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    OrderDetailRepository orderDetailRepository;

    @GetMapping("/orderdetails")
    public List<OrderDetail> getAllOrderDetails() {
        return orderDetailService.getAllOrderDetails();
    }

    @GetMapping("/orderdetails/orderId/{orderId}")
    public ResponseEntity<Object> getOrderDetailsByOrderId(@PathVariable Long orderId) {
        List<OrderDetail> listOrderDetail = orderDetailService.getOrderDetailsByOrderId(orderId);
        if(listOrderDetail != null) {
            return ResponseEntity.ok(listOrderDetail);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/orderdetails/productId/{productId}")
    public ResponseEntity<Object> getOrderDetailsByProductId(@PathVariable Long productId) {
        List<OrderDetail> listOrderDetail = orderDetailService.getOrderDetailsByProductId(productId);
        if(listOrderDetail != null) {
                return ResponseEntity.ok(listOrderDetail);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/orderdetails/{id}")
    public ResponseEntity<Object> getOrderDetailById(@PathVariable Long id) {
        OrderDetail existedOrderDetail = orderDetailService.getOrderDetailById(id);
        if(existedOrderDetail != null) {
            return ResponseEntity.ok(existedOrderDetail);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/orderdetails/orderId/{orderId}/productId/{productId}")
    public ResponseEntity<Object> createOrderDetail(
        @PathVariable Long orderId,  @PathVariable Long productId,
        @RequestBody OrderDetail pOrderDetail) {
        OrderDetail newOrderDetail = orderDetailService.createOrderDetail(orderId, productId, pOrderDetail);
        if(newOrderDetail != null) {
            try {
                OrderDetail savedOrderDetail = orderDetailRepository.save(newOrderDetail);
                return new ResponseEntity<>(savedOrderDetail, HttpStatus.CREATED);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
                    .body(ex.getCause().getMessage());
            }
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/orderdetails/{id}/productId/{productId}")
    public ResponseEntity<Object> updateOrderDetailById(
        @PathVariable Long id, @PathVariable Long productId,
        @RequestBody OrderDetail pOrderDetail) {
        OrderDetail updatedOrderDetail = orderDetailService.updateOrderDetailById(id, productId, pOrderDetail);
        if(updatedOrderDetail != null) {
            try {
                OrderDetail savedOrderDetail = orderDetailRepository.save(updatedOrderDetail);
                return ResponseEntity.ok(savedOrderDetail);
            } catch(Exception ex) {
                return ResponseEntity.unprocessableEntity()
                    .body(ex.getCause().getMessage());
            }
        } else 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @Transactional
    @DeleteMapping("/orderdetails/{id}")  
	public ResponseEntity<Object> deleteOrderDetailById(@PathVariable Long id){
		return orderDetailService.deleteOrderDetailById(id);  
    }
}
