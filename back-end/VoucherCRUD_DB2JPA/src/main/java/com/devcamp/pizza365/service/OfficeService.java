package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;

@Service
public class OfficeService {
    @Autowired
    IOfficeRepository iOfficeRepository;
    
    public List<Office> getAllOffices() {
        List<Office> listOffice = new ArrayList<Office>();
        iOfficeRepository.findAll().forEach(listOffice::add);
        return listOffice;
    }

    public Office getOfficeById(Long id) {
        Optional<Office> existedOffice = iOfficeRepository.findById(id);
        if(existedOffice.isPresent()) {
            return existedOffice.get();
        } else {
            return null;
        }
    }

    public Office createOffice(Office pOffice) {
        Office newOffice = new Office();
        newOffice.setAddressLine(pOffice.getAddressLine());
        newOffice.setCity(pOffice.getCity());
        newOffice.setCountry(pOffice.getCountry());
        newOffice.setPhone(pOffice.getPhone());
        newOffice.setState(pOffice.getState());
        newOffice.setTerritory(pOffice.getTerritory());
        return newOffice;
    }

    public Office updateOfficeById(Long id, Office pOffice) {
        Optional<Office> existedOffice = iOfficeRepository.findById(id);
        if(existedOffice.isPresent()) {
            Office updatedOffice = existedOffice.get();
            updatedOffice.setAddressLine(pOffice.getAddressLine());
            updatedOffice.setCity(pOffice.getCity());
            updatedOffice.setCountry(pOffice.getCountry());
            updatedOffice.setPhone(pOffice.getPhone());
            updatedOffice.setState(pOffice.getState());
            updatedOffice.setTerritory(pOffice.getTerritory());
            return updatedOffice;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deleteOfficeById(Long id) {
        try {
            iOfficeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }

    public ResponseEntity<Object> deleteOffices() {
        try {
            iOfficeRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
