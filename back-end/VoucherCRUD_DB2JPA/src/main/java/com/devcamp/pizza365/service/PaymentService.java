package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IPaymentRepository;

@Service
public class PaymentService {
    @Autowired
    ICustomerRepository iCustomerRepository;

    @Autowired
    IPaymentRepository iPaymentRepository;

    public List<Payment> getAllPayments() {
        List<Payment> listPayment = new ArrayList<Payment>();
        iPaymentRepository.findAll().forEach(listPayment::add);
        return listPayment;
    }

    public List<Payment> getPaymentsByCustomerId(Long CustomerId) {
        Optional<Customer> existedCustomer = iCustomerRepository.findById(CustomerId);
        if(existedCustomer.isPresent()) {
            List<Payment> listPayment = new ArrayList<Payment>();
            existedCustomer.get().getPayments().forEach(listPayment::add);
            return listPayment;
        } else {
            return null;
        }
    }

    public Payment getPaymentById(Long id) {
        Optional<Payment> existedPayment = iPaymentRepository.findById(id);
        if(existedPayment.isPresent()) {
            return existedPayment.get();
        } else {
            return null;
        }
    }

    public Payment createPayment(Long customerId, Payment pPayment) {
        Optional<Customer> existedCustomer = iCustomerRepository.findById(customerId);
        if(existedCustomer.isPresent()) {
            Payment newPayment = new Payment();
            newPayment.setAmmount(pPayment.getAmmount());
            newPayment.setCheckNumber(pPayment.getCheckNumber());
            newPayment.setPaymentDate(pPayment.getPaymentDate());
            newPayment.setCustomer(existedCustomer.get());
            return newPayment;
        } else {
            return null;
        }
    }

    public Payment updatePaymentById(Long id, Payment pPayment) {
        Optional<Payment> existedPayment = iPaymentRepository.findById(id);
        if(existedPayment.isPresent()) {
            Payment updatedPayment = existedPayment.get();
            updatedPayment.setAmmount(pPayment.getAmmount());
            updatedPayment.setCheckNumber(pPayment.getCheckNumber());
            updatedPayment.setPaymentDate(pPayment.getPaymentDate());
            return updatedPayment;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> deletePaymentById(Long id) {
        try {
            iPaymentRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.unprocessableEntity()
                .body(e.getCause().getMessage());
        }
    }

}
