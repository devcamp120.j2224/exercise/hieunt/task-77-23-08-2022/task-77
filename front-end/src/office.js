$(document).ready(function () {
    // Vùng 1: Khai báo biến global
    var gId;
    var gArrayOffice;
    const gCOL_NAME = ["id", "city", "phone", "addressLine", "state", "country", "territory", "action"];
    const gCOL_ID = 0;
    const gCITY_COL = 1;
    const gPHONE_COL = 2;
    const gADDRESS_LINE_COL = 3;
    const gSTATE_COL = 4;
    const gCOUNTRY_COL = 6;
    const gTERRITORY_COL = 5;
    const gACTION_COL = 7;


    var table = $("#office-table").DataTable({
        "columns": [
            { "data": gCOL_NAME[gCOL_ID] },
            { "data": gCOL_NAME[gCITY_COL] },
            { "data": gCOL_NAME[gPHONE_COL] },
            { "data": gCOL_NAME[gADDRESS_LINE_COL] },
            { "data": gCOL_NAME[gSTATE_COL] },
            { "data": gCOL_NAME[gCOUNTRY_COL] },
            { "data": gCOL_NAME[gTERRITORY_COL] },
            { "data": gCOL_NAME[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
                                <i class="fas fa-trash text-danger" id="btn-delete"></i>`
            }
        ]
    })

    // Vùng 2: Vùng gán
    onPageLoading();
    $("#create-office").on('click', function () {
        onBtnCreateClick();
    });

    $("#office-table").on("click", "#btn-update", function () {
        onBtnUpdateTableClick(this);
    })

    $("#btn-update-office").on("click", function () {
        onBtnUpdateModalClick();
    })

    $('#office-table').on("click", "#btn-delete", function () {
        onBtnDeleteTableClick(this);
    })

    $('#delete-modal-office').on('click', function () {
        onBtnDeleteDataClick();
    })



    // Vùng 3: Khai báo hàm xử lý sự kiện
    function onPageLoading() {
        getAllOffice();
        loadDataToTable();
    }

    function onBtnCreateClick() {
        var vOfficeObj = {
            city: "",
            phone: "",
            addressLine: "",
            state: "",
            country: "",
            territory: ""
        };
        getDataOffice(vOfficeObj);
        var vCheck = validateDataOffice(vOfficeObj);
        if (vCheck) {
            callApiCreateOffice(vOfficeObj);
        }
    }

    function onBtnUpdateTableClick(paramEle) {
        "use strict"
        $('#office-modal').modal("show");
        var vTableRow = $(paramEle).closest("tr");
        var vDataRow = table.row(vTableRow).data();
        gId = vDataRow.id;
        $("#input-modal-office-id").val(gId);
        $("#input-modal-city").val(vDataRow.city);
        $("#input-modal-phone").val(vDataRow.phone);
        $("#input-modal-address-line").val(vDataRow.addressLine);
        $("#input-modal-state").val(vDataRow.state);
        $("#input-modal-country").val(vDataRow.country);
        $("#input-modal-territory").val(vDataRow.territory);
    }

    function onBtnUpdateModalClick() {
        "use strict";
        var vOfficeObj = {
            city: "",
            phone: "",
            addressLine: "",
            state: "",
            country: "",
            territory: ""
        };
        getDataUpdateOffice(vOfficeObj);
        var vCheck = validateDataOffice(vOfficeObj);
        if (vCheck) {
            callApiUpdateOffice(vOfficeObj);
            $("#office-modal").modal("hide");
        }

    }
    function onBtnDeleteTableClick(paramEle) {
        "use strict";
        $('#modal-delete-office').modal("show");
        var vTableRow = $(paramEle).closest("tr");
        var vDataRow = table.row(vTableRow).data();
        gId = vDataRow.id;
    }

    // Vùng 4: Khai báo hàm dùng chung
    function getDataOffice(paramObj) {
        "use strict"
        paramObj.city = $("#input-city").val().trim();
        paramObj.phone = $('#input-phone').val().trim();
        paramObj.addressLine = $('#input-address-line').val().trim();
        paramObj.state = $("#input-state").val().trim();
        paramObj.country = $('#input-country').val().trim();
        paramObj.territory = $('#input-territory').val().trim();
    }

    function getDataUpdateOffice(paramObj) {
        "use strict"
        paramObj.city = $("#input-modal-city").val().trim();
        paramObj.phone = $('#input-modal-phone').val().trim();
        paramObj.addressLine = $('#input-modal-address-line').val().trim();
        paramObj.state = $("#input-modal-state").val().trim();
        paramObj.country = $('#input-modal-country').val().trim();
        paramObj.territory = $('#input-modal-territory').val().trim();
    }

    function validateDataOffice(paramData) {
        "use strict"
        var vResult = false;
        if (paramData.city == "") {
            alert("Chưa nhập City!");
        }
        else if (paramData.phone == "") {
            alert("Chưa nhập Phone!");
        }
        else if (paramData.addressLine == "") {
            alert("Chưa nhập Address Line!");
        }
        else if (paramData.country == "") {
            alert("Chưa nhập Country!");
        }
        else vResult = true;
        return vResult;
    }

    function callApiCreateOffice(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/offices",
            data: JSON.stringify(paramDataObj),
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                console.log(res);
                getAllOffice();
                loadDataToTable();
                clearInpCreate();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function callApiUpdateOffice(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/offices/" + gId,
            data: JSON.stringify(paramDataObj),
            type: "PUT",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                console.log(res);
                getAllOffice();
                loadDataToTable();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function onBtnDeleteDataClick() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/offices" + "/" + gId,
            type: "DELETE",
            success: function (res) {
                $('#modal-delete-office').modal('hide');
                console.log(res);
                getAllOffice();
                loadDataToTable();
            },
            error: function (err) {
                console.log(err.status);
            }
        })
    }

    function clearInpCreate() {
        "use strict"
        $('#input-city').val("");
        $('#input-phone').val("");
        $('#input-address-line').val("");
        $('#input-state').val("");
        $('#input-country').val("");
        $('#input-territory').val("");
    }

    function getAllOffice() {
        $.ajax({
            url: "http://localhost:8080/offices",
            type: "GET",
            dataType: "json",
            async: false,
            success: function (res) {
                console.log(res);
                gArrayOffice = res
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function loadDataToTable() {
        table.clear();
        table.rows.add(gArrayOffice);
        table.draw();
    }
})