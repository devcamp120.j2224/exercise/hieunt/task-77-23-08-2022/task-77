$(document).ready(function () {
    // Vùng 1: Khai báo biến global
    var gId;
    var gArrayOrder;
    var gArrayCustomer;
    const gCOL_NAME = ["id","customerFullName", "comments", "status", "orderDate", "requiredDate","shippedDate", "action"];
    const gCOL_ID = 0;
    const gCUSTOMER_COL = 1;
    const gCOMMENTS_COL = 2;
    const gSTATUS_COL = 3;
    const gORDER_DATE_COL = 4;
    const gREQUIRED_DATE_COL = 5;
    const gSHIPPED_DATE_COL = 6;
    const gACTION_COL = 7;


    var table = $("#order-table").DataTable({
        "columns": [
            { "data": gCOL_NAME[gCOL_ID] },
            { "data": gCOL_NAME[gCUSTOMER_COL] },
            { "data": gCOL_NAME[gCOMMENTS_COL] },
            { "data": gCOL_NAME[gSTATUS_COL] },
            { "data": gCOL_NAME[gORDER_DATE_COL] },
            { "data": gCOL_NAME[gREQUIRED_DATE_COL] },
            { "data": gCOL_NAME[gSHIPPED_DATE_COL] },
            { "data": gCOL_NAME[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
                                | <i class="fas fa-trash text-danger" id="btn-delete"></i>
                                | <i class="fas fa-info" id="btn-detail-order"></i>`
            },
        ]
    })

    // Vùng 2: Vùng gán
    onPageLoading();
    $("#create-order").on('click', function () {
        onBtnCreateClick();
    });

    $("#order-table").on("click", "#btn-update", function () {
        $('#modal-update-order').modal("show");
        onBtnUpdateTableClick(this);
    })

    $("#update-order").on("click", function () {
        onBtnUpdateModalClick();
    })


    $('#order-table').on("click", "#btn-delete", function () {
        $('#modal-delete-order').modal("show");
        onBtnDeleteTableClick(this);
    })

    $('#btn-confirm-delete-order').on('click', function () {
        onBtnDeleteDataClick();
    })
    $("#order-table").on("click","#btn-detail-order",function(){
        onBtnDetailOrderClick(this);
    })
    $("#btn-filter").on("click",onBtnFilterClick)

    // Vùng 3: Khai báo hàm xử lý sự kiện
    function onPageLoading() {
        getAllCustomer();
        getAllOrder();
        loadDataToTable();
    }

    function onBtnCreateClick() {
        var vOrderObj = {
            customer: {
                id: ""
            },
            orderDate: "",
            requiredDate: "",
            shippedDate: "",
            status: "",
            comments: ""
        };
        getDataOrder(vOrderObj);
        var vCheck = validateDataOrder(vOrderObj);
        if (vCheck) {
            callApiCreateOrder(vOrderObj);
        }
    }

    function onBtnUpdateTableClick(paramEle){
        "use strict"
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
        $("#inp-modal-orderId").val(gId);
        $("#inp-modal-comments").val(vDataRow.comments);
        $("#inp-modal-status").val(vDataRow.status);
        $("#inp-modal-orderDate").val(vDataRow.orderDate);
        $("#inp-modal-requiredDate").val(vDataRow.requiredDate);
        $("#inp-modal-shippedDate").val(vDataRow.shippedDate);
        $("#select-modal-customer").val(vDataRow.customerId).prop("selected",true);
    }

    function onBtnUpdateModalClick(){
        "use strict";
        var vOrderObj = {
            customer: {
                id: ""
            },
            orderDate: "",
            requiredDate: "",
            shippedDate: "",
            status: "",
            comments: ""
        };
        getDataUpdateOrder(vOrderObj);
        var vCheck = validateDataOrder(vOrderObj);
        if (vCheck) {
            callApiUpdateOrder(vOrderObj);
            $("#modal-update-order").modal("hide");
        }

    }
    function onBtnDeleteTableClick(paramEle) {
        "use strict";
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
    }

    function onBtnDetailOrderClick(paramEle){
        "use strict";
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
        window.location.href = "orderOrderDetail.html?id=" + gId;        
    }

    function onBtnFilterClick(){
        "use strict"
        var vFilterCustomerId = $("#select-filter-customer").val();
        callApiGetOrdersByCustomerId(vFilterCustomerId);
        loadDataToTable();
    }

    // Vùng 4: Khai báo hàm dùng chung
    function getDataOrder(paramObj) {
        "use strict"
        var today = new Date().toISOString();
        paramObj.customer.id = $('#select-customer').find(":selected").val()
        paramObj.comments = $('#inp-comments').val().trim();
        paramObj.status = $('#inp-status').val().trim();
        paramObj.requiredDate = today;
        paramObj.shippedDate = today;
        paramObj.orderDate = today;
        $('#inp-orderDate').val(today);
        $("#inp-requiredDate").val(today);
        $("#inp-shippedDate").val(today);  
    }

    function getDataUpdateOrder(paramObj){
        "use strict"
        var today = new Date().toISOString();
        paramObj.customer.id = $('#select-modal-customer').find(":selected").val()
        paramObj.comments = $('#inp-modal-comments').val().trim();
        paramObj.status = $('#inp-modal-status').val().trim();
        paramObj.requiredDate = today;
        paramObj.shippedDate = today;
        paramObj.orderDate = today;
        $('#inp-modal-orderDate').val(today);
        $("#inp-modal-requiredDate").val(today);
        $("#inp-modal-shippedDate").val(today); 
    }

    function validateDataOrder(paramData) {
        "use strict"
        var vIsValid = false;
        if (paramData.customer.id == "") {
            alert("Vui lòng chọn Customer!");
        }
        else if (paramData.comments == "") {
            alert("Chưa nhập comments!");
        }
        else if (paramData.status == "") {
            alert("Chưa nhập status!");
        }
        else vIsValid = true;
        return vIsValid;
    }

    function callApiCreateOrder(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/orders/customerId/" + paramDataObj.customer.id,
            data: JSON.stringify(paramDataObj),
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            async: false,
            success: function (res) {
                getAllOrder();
                loadDataToTable();
                clearInpCreate();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function callApiUpdateOrder(paramDataObj){
        "use strict"
        $.ajax({
            url: "http://localhost:8080/orders/" + gId,
            data: JSON.stringify(paramDataObj),
            type: "PUT",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                getAllOrder();
                loadDataToTable();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function onBtnDeleteDataClick() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/orders" + "/" + gId,
            type: "DELETE",
            success: function (res) {
                $('#modal-delete-order').modal('hide');
                getAllOrder();
                loadDataToTable();
            },
            error: function (err) {
                console.log(err.status);
            }
        })
    }

    function clearInpCreate() {
        "use strict"
        $('#inp-comments').val("");
        $('#inp-orderDate').val("");
        $('#inp-requiredDate').val("");
        $('#inp-shippedDate').val("");
        $('#inp-status').val("");
        $('#select-customer').val("");
    }

    function getAllCustomer() {
        $.ajax({
            url: "http://localhost:8080/customers",
            type: "GET",
            dataType: "json",
            // async: false,
            success: function (res) {
                console.log(res);
                gArrayCustomer = res;
                loadDataCustomerToSelect();
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function getAllOrder() {
        $.ajax({
            url: "http://localhost:8080/orders",
            type: "GET",
            dataType: "json",
            async: false,
            success: function (res) {
                console.log(res);
                gArrayOrder = res;
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function loadDataToTable() {
        table.clear();
        table.rows.add(gArrayOrder);
        table.draw();
    }

    function loadDataCustomerToSelect() {
        "use strict";
        for (var i = 0; i < gArrayCustomer.length; i++) {
            $('#select-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
            $('#select-modal-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
            $("#select-filter-customer").append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
        }
    }

    function callApiGetOrdersByCustomerId(paramCustomerId){ 
        $.ajax({
            url:"http://localhost:8080/orders/customerId/" + paramCustomerId,
            type:"GET",
            data:"json",
            async:false,
            success:function(res){
                gArrayOrder = res;
            },
            error:function(err){
                console.log(err.status);
            }
        })
    }
})