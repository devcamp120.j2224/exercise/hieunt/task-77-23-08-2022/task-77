$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/orders";
    var gOrderId = 0;
    var gCustomerId; 
    var gCustomerName = "";
    var gArrayOrder;
    const gID_COL = 0;
    const gCOMMENTS_COL = 1;
    const gORDER_DATE_COL = 2;
    const gREQUIRED_DATE_COL = 3;
    const gSHIPPED_DATE_COL = 4;
    const gSTATUS_COL = 5;
    const gCUSTOMER_NAME_COL = 6;
    const gACTION_COL = 7;
    var gNameCol = ["id", "comments", "orderDate", "requiredDate", "shippedDate", "status", "customerFullName", "action"];
    var gTableOrder = $('#order-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gCOMMENTS_COL] },
        { data: gNameCol[gORDER_DATE_COL] },
        { data: gNameCol[gREQUIRED_DATE_COL] },
        { data: gNameCol[gSHIPPED_DATE_COL] },
        { data: gNameCol[gSTATUS_COL] },
        { data: gNameCol[gCUSTOMER_NAME_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ],
      searching: false
    });
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#create-order').on('click', onBtnCreateOrderClick);
    $('#order-table').on('click', "#btn-update", function() {
      onBtnUpdateOrderClick(this);
    });
    $('#update-order').on('click', onBtnUpdateModalClick);
    $('#order-table').on('click', "#btn-delete", function() {
      onBtnDeleteOrderClick(this);
    });
    $('#delete-order').on('click', onBtnDeleteConfirmClick);
  
    $("#back-customer").on("click",onBtnBackClick)
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      getGCustomerId()
      callApiGetCustomerById();
      callApiGetOrdersByCustomerId();
      loadDataToTable();
    }
    // Hàm xử lý khi click btn create new order
    function onBtnCreateOrderClick() {
      var vOrderObj = {
        comments: "",
        orderDate: "",
        requiredDate: "",
        shippedDate: "",
        status: "",
        customer: {
          id: gCustomerId
        }
      };
      getDataFromInputCreate(vOrderObj);
      // console.log(vOrderObj);
      var vIsCheck = validateData(vOrderObj);
      if (vIsCheck == true) {
        // console.log(vOrderObj);
        callApiPostNewOrder(vOrderObj);
      }
    }
    // Hàm xử lý khi click btn update order
    function onBtnUpdateOrderClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrder.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderId = vDataRow.id;
      $('#modal-update-order').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
      var vOrderObj = {
        comments: "",
        orderDate: "",
        requiredDate: "",
        shippedDate: "",
        status: "",
        customer: {
          id: gCustomerId
        }
      };
      getDataFromInputUpdate(vOrderObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vOrderObj);
      if (vIsCheck == true) {
        callApiPutOrderById(vOrderObj);
        $('#modal-update-order').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete order
    function onBtnDeleteOrderClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrder.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderId = vDataRow.id;
      $('#modal-delete-order').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeleteOrder();
      $('#modal-delete-order').modal("hide");
    }

    function onBtnBackClick(){
      "use strict"
      window.location.href="customer.html";
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    function getGCustomerId() {
      var vHref = new URL(window.location);
      gCustomerId = vHref.searchParams.get("id");
      console.log("Customer Id = " + gCustomerId);
      if (gCustomerId == null) {
        alert("Không có Customer Id!");
        onBtnBackClick();
      }
    }
    // Hàm call api get all
    function callApiGetOrdersByCustomerId() {
      $.ajax({
        url: "http://localhost:8080/orders/customerId/" + gCustomerId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayOrder = res;
          console.log(gArrayOrder);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTableOrder.clear();
      gTableOrder.rows.add(gArrayOrder);
      gTableOrder.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
      var today = new Date().toISOString();
      paramObj.comments = $('#inp-comments').val().trim();
      paramObj.orderDate = today;
      paramObj.requiredDate = today;
      paramObj.shippedDate = today;
      $('#inp-orderDate').val(today);
      $('#inp-requiredDate').val(today);
      $('#inp-shippedDate').val(today);
      paramObj.status = $('#inp-status').val().trim();
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      var vIsValid = false;
      if (paramObj.customer.id == "") {
        alert("Chưa chọn Customer!");
      }
      else if (paramObj.comments == "") {
        alert("Chưa nhập comments!");
      }
      else if (paramObj.status == "") {
        alert("Chưa nhập status!");
      }
      else vIsValid = true;
      return vIsValid;
    }
    // Hàm call api post
    function callApiPostNewOrder(paramObj) {
      $.ajax({
        url: gUrl + "/customerId/" + gCustomerId,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          callApiGetOrdersByCustomerId();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
      $('#inp-comments').val("");
      $('#inp-orderDate').val("");
      $('#inp-requiredDate').val("");
      $('#inp-shippedDate').val("");
      $('#inp-status').val("");
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-orderId').val(gOrderId)
      $('#inp-modal-comments').val(paramData.comments);
      $('#inp-modal-orderDate').val(paramData.orderDate);
      $('#inp-modal-requiredDate').val(paramData.requiredDate);
      $('#inp-modal-shippedDate').val(paramData.shippedDate);
      $('#inp-modal-status').val(paramData.status);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
      paramObj.comments = $('#inp-modal-comments').val().trim();
      paramObj.orderDate = $('#inp-modal-orderDate').val();
      paramObj.requiredDate = $('#inp-modal-requiredDate').val();
      paramObj.shippedDate = $('#inp-modal-shippedDate').val();
      paramObj.status = $('#inp-modal-status').val().trim();
    }
    // Hàm call api put
    function callApiPutOrderById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gOrderId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetOrdersByCustomerId();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeleteOrder() {
      $.ajax({
        url: gUrl + "/" + gOrderId,
        type: "DELETE",
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetOrdersByCustomerId();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm get api lấy tt customer
    function callApiGetCustomerById() {
      $.ajax({
        url: "http://localhost:8080/customers/" + gCustomerId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gCustomerName = res.firstName + " " + res.lastName;
          console.log(res);
          console.log(gCustomerName);
          loadDataToSelectCustomer();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load select customer
    function loadDataToSelectCustomer() {
      $('#select-customer').append($('<option>').val(gCustomerId).html(gCustomerName)).val(gCustomerId);
      $('#select-modal-customer').append($('<option>').val(gCustomerId).html(gCustomerName)).val(gCustomerId);
    }
  });