$(document).ready(function () {
    // Vùng 1: Khai báo biến global
    var gId;
    var gArrayProductLine;
    const gCOL_NAME = ["id", "productLine", "description", "action"];
    const gCOL_ID = 0;
    const gPRODUCT_LINE_COL = 1;
    const gDESCRIPTION_COL = 2;
    const gACTION_COL = 3;


    var table = $("#product-line-table").DataTable({
        "columns": [
            { "data": gCOL_NAME[gCOL_ID] },
            { "data": gCOL_NAME[gPRODUCT_LINE_COL] },
            { "data": gCOL_NAME[gDESCRIPTION_COL] },
            { "data": gCOL_NAME[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
                                <i class="fas fa-trash text-danger" id="btn-delete"></i>`
            }
        ]
    })

    // Vùng 2: Vùng gán
    onPageLoading();
    $("#create-product-line").on('click', function () {
        onBtnCreateClick();
    });

    $("#product-line-table").on("click", "#btn-update", function () {
        $('#product-line-modal').modal("show");
        onBtnUpdateTableClick(this);
    })

    $("#btn-update-product-line").on("click", function () {
        onBtnUpdateModalClick();
    })


    $('#product-line-table').on("click", "#btn-delete", function () {
        $('#modal-delete-product-line').modal("show");
        onBtnDeleteTableClick(this);
    })

    $('#delete-modal-product-line').on('click', function () {
        onBtnDeleteDataClick();
    })



    // Vùng 3: Khai báo hàm xử lý sự kiện
    function onPageLoading() {
        getAllProductLine();
        loadDataToTable();
    }

    function onBtnCreateClick() {
        var vProductLineObj = {
            productLine: "",
            description: "",
        };
        getDataProductLine(vProductLineObj);
        var vCheck = validateDataProductLine(vProductLineObj);
        if (vCheck) {
            callApiCreateProductLine(vProductLineObj);
        }
    }

    function onBtnUpdateTableClick(paramEle) {
        "use strict"
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
        $("#input-modal-product-line-id").val(gId);
        $("#input-modal-product-line").val(vDataRow.productLine);
        $("#input-modal-description").val(vDataRow.description);
    }

    function onBtnUpdateModalClick() {
        "use strict";
        var vProductLineObj = {
            productLine: "",
            description: "",
        };
        getDataUpdateProductLine(vProductLineObj);
        var vCheck = validateDataProductLine(vProductLineObj);
        if (vCheck) {
            callApiUpdateProductLine(vProductLineObj);
            $("#product-line-modal").modal("hide");
        }

    }
    function onBtnDeleteTableClick(paramEle) {
        "use strict";
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
    }

    // Vùng 4: Khai báo hàm dùng chung
    function getDataProductLine(paramObj) {
        "use strict"
        paramObj.productLine = $("#input-product-line").val().trim();
        paramObj.description = $('#input-description').val().trim();
    }

    function getDataUpdateProductLine(paramObj) {
        "use strict"
        paramObj.productLine = $("#input-modal-product-line").val().trim();
        paramObj.description = $('#input-modal-description').val().trim();
    }

    function validateDataProductLine(paramData) {
        "use strict"
        var vResult = false;
        if (paramData.productLine == "") {
            alert("Chưa nhập product line!");
        }
        else if (paramData.description == "") {
            alert("Chưa nhập description!");
        }
        else vResult = true;
        return vResult;
    }

    function callApiCreateProductLine(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/productlines",
            data: JSON.stringify(paramDataObj),
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                getAllProductLine();
                loadDataToTable();
                clearInpCreate();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function callApiUpdateProductLine(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/productlines/" + gId,
            data: JSON.stringify(paramDataObj),
            type: "PUT",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                getAllProductLine();
                loadDataToTable();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function onBtnDeleteDataClick() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/productlines/" + gId,
            type: "DELETE",
            async: false,
            success: function (res) {
                console.log(res);
                $('#modal-delete-product-line').modal('hide');
                getAllProductLine();
                loadDataToTable();
            },
            error: function (err) {
                console.log(err);
            }
        })
    }

    function clearInpCreate() {
        "use strict"
        $('#input-product-line').val("");
        $('#input-description').val("");
    }

    function getAllProductLine() {
        $.ajax({
            url: "http://localhost:8080/productlines",
            type: "GET",
            dataType: "json",
            async: false,
            success: function (res) {
                console.log(res);
                gArrayProductLine = res
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function loadDataToTable() {
        table.clear();
        table.rows.add(gArrayProductLine);
        table.draw();
    }
})