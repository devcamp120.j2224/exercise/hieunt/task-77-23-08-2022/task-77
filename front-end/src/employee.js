$(document).ready(function () {
    // Vùng 1: Khai báo biến global
    var gId;
    var gArrayEmployee;
    const gCOL_NAME = ["id", "firstName", "lastName", "extension", "email", "officeCode", "reportTo", "jobTitle", "action"];
    const gCOL_ID = 0;
    const gFIRST_NAME_COL = 1;
    const gLAST_NAME_COL = 2;
    const gEXTENSION_COL = 3;
    const gEMAIL_COL = 4;
    const gOFFICE_CODE_COL = 5;
    const gREPORT_TO_COL = 6;
    const gJOB_TITLE_COL = 7;
    const gACTION_COL = 8;


    var table = $("#employee-table").DataTable({
        "columns": [
            { "data": gCOL_NAME[gCOL_ID] },
            { "data": gCOL_NAME[gFIRST_NAME_COL] },
            { "data": gCOL_NAME[gLAST_NAME_COL] },
            { "data": gCOL_NAME[gEXTENSION_COL] },
            { "data": gCOL_NAME[gEMAIL_COL] },
            { "data": gCOL_NAME[gOFFICE_CODE_COL] },
            { "data": gCOL_NAME[gREPORT_TO_COL] },
            { "data": gCOL_NAME[gJOB_TITLE_COL] },
            { "data": gCOL_NAME[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
                                <i class="fas fa-trash text-danger" id="btn-delete"></i>`
            }
        ]
    })

    // Vùng 2: Vùng gán
    onPageLoading();
    $("#create-employee").on('click', function () {
        onBtnCreateClick();
    });

    $("#employee-table").on("click", "#btn-update", function () {
        $('#employee-modal').modal("show");
        onBtnUpdateTableClick(this);
    })

    $("#btn-update-employee").on("click", function () {
        onBtnUpdateModalClick();
    })


    $('#employee-table').on("click", "#btn-delete", function () {
        $('#modal-delete-employee').modal("show");
        onBtnDeleteTableClick(this);
    })

    $('#delete-modal-employee').on('click', function () {
        onBtnDeleteDataClick();
    })



    // Vùng 3: Khai báo hàm xử lý sự kiện
    function onPageLoading() {
        getAllEmployee();
        loadDataToTable();
    }

    function onBtnCreateClick() {
        var vEmployeeObj = {
            lastName: "",
            firstName: "",
            extension: "",
            email: "",
            officeCode: "",
            reportTo: "",
            jobTitle: ""
        };
        getDataEmployee(vEmployeeObj);
        var vCheck = validateDataEmployee(vEmployeeObj);
        if (vCheck) {
            callApiCreateEmployee(vEmployeeObj);
        }
    }

    function onBtnUpdateTableClick(paramEle) {
        "use strict"
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
        $("#input-modal-employee-id").val(gId);
        $("#input-modal-first-name").val(vDataRow.firstName);
        $("#input-modal-last-name").val(vDataRow.lastName);
        $("#input-modal-extension").val(vDataRow.extension);
        $("#input-modal-email").val(vDataRow.email);
        $("#input-modal-office-code").val(vDataRow.officeCode);
        $("#input-modal-report-to").val(vDataRow.reportTo);
        $("#input-modal-job-title").val(vDataRow.jobTitle);
    }

    function onBtnUpdateModalClick() {
        "use strict";
        var vEmployeeObj = {
            lastName: "",
            firstName: "",
            extension: "",
            email: "",
            officeCode: "",
            reportTo: "",
            jobTitle: ""
        };
        getDataUpdateEmployee(vEmployeeObj);
        var vCheck = validateDataEmployee(vEmployeeObj);
        if (vCheck) {
            callApiUpdateEmployee(vEmployeeObj);
            $("#employee-modal").modal("hide");
        }

    }
    function onBtnDeleteTableClick(paramEle) {
        "use strict";
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
    }

    // Vùng 4: Khai báo hàm dùng chung
    function getDataEmployee(paramObj) {
        "use strict"
        paramObj.lastName = $("#input-last-name").val().trim();
        paramObj.firstName = $('#input-first-name').val().trim();
        paramObj.extension = $('#input-extension').val().trim();
        paramObj.email = $("#input-email").val().trim();
        paramObj.officeCode = $('#input-office-code').val().trim();
        paramObj.reportTo = $('#input-report-to').val().trim();
        paramObj.jobTitle = $('#input-job-title').val().trim();
    }

    function getDataUpdateEmployee(paramObj) {
        "use strict"
        paramObj.lastName = $("#input-modal-last-name").val().trim();
        paramObj.firstName = $('#input-modal-first-name').val().trim();
        paramObj.extension = $('#input-modal-extension').val().trim();
        paramObj.email = $("#input-modal-email").val().trim();
        paramObj.officeCode = $('#input-modal-office-code').val().trim();
        paramObj.reportTo = $('#input-modal-report-to').val().trim();
        paramObj.jobTitle = $('#input-modal-job-title').val().trim();
    }

    function validateDataEmployee(paramData) {
        "use strict"
        var vResult = false;
        var vRegex = (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        if (paramData.firstName == "") {
            alert("Chưa nhập First name!");
        }
        else if (paramData.lastName == "") {
            alert("Chưa nhập Last name!");
        }
        else if (paramData.extension == "") {
            alert("Chưa nhập Extension!");
        }
        else if (paramData.email == "") {
            alert("Chưa nhập Email!");
        }
        else if (!vRegex.test(paramData.email)) {
            alert("Không đúng định dạng Email!");
        }
        else if (paramData.officeCode == "") {
            alert("Chưa nhập Office code!");
        }
        else if (paramData.officeCode <= 0 || Number.isInteger(Number(paramData.officeCode)) == false) {
            alert("Office code phải là số nguyên dương!");
        }
        else if (paramData.reportTo == "") {
            alert("Chưa nhập Report to!");
        }
        else if (paramData.reportTo < 0 || Number.isInteger(Number(paramData.reportTo)) == false) {
            alert("Report to phải là số nguyên từ 0 trở lên!");
        }
        else if (paramData.jobTitle == "") {
            alert("Chưa nhập Job title!");
        }
        else 
            vResult = true;
        return vResult;
    }

    function callApiCreateEmployee(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/employees",
            data: JSON.stringify(paramDataObj),
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                console.log(res);
                getAllEmployee();
                loadDataToTable();
                clearInpCreate();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function callApiUpdateEmployee(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/employees/" + gId,
            data: JSON.stringify(paramDataObj),
            type: "PUT",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                console.log(res);
                getAllEmployee();
                loadDataToTable();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function onBtnDeleteDataClick() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/employees" + "/" + gId,
            type: "DELETE",
            success: function (res) {
                console.log(res);
                $('#modal-delete-employee').modal('hide');
                getAllEmployee();
                loadDataToTable();
            },
            error: function (err) {
                console.log(err.status);
            }
        })
    }

    function clearInpCreate() {
        "use strict"
        $('#input-first-name').val("");
        $('#input-last-name').val("");
        $('#input-email').val("");
        $('#input-extension').val("");
        $('#input-office-code').val("");
        $('#input-report-to').val("");
        $('#input-job-title').val("");
    }

    function getAllEmployee() {
        $.ajax({
            url: "http://localhost:8080/employees",
            type: "GET",
            dataType: "json",
            async: false,
            success: function (res) {
                console.log(res);
                gArrayEmployee = res
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function loadDataToTable() {
        table.clear();
        table.rows.add(gArrayEmployee);
        table.draw();
    }
})