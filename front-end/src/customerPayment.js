$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/payments";
    var gPaymentId = 0;
    var gCustomerId;
    var gCustomerName = "";
    var gArrayPayment;
    const gID_COL = 0;
    const gCHECK_NUM_COL = 1;
    const gPAYMENT_DATE_COL = 2;
    const gAMMOUNT_COL = 3;
    const gCUSTOMER_NAME_COL = 4;
    const gACTION_COL = 5;
    var gNameCol = ["id", "checkNumber", "paymentDate", "ammount", "customerFullName", "action"];
    var gTablePayment = $('#payments-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gCHECK_NUM_COL] },
        { data: gNameCol[gPAYMENT_DATE_COL] },
        { data: gNameCol[gAMMOUNT_COL] },
        { data: gNameCol[gCUSTOMER_NAME_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ]
    });
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#create-payment').on('click', onBtnCreatePaymentClick);
    $('#payments-table').on('click', "#btn-update", function() {
      onBtnUpdatePaymentClick(this);
    });
    $('#update-payment').on('click', onBtnUpdateModalClick);
    $('#payments-table').on('click', "#btn-delete", function() {
      onBtnDeletePaymentClick(this);
    });
    $('#delete-payment').on('click', onBtnDeleteConfirmClick);
  
    $("#back-customer").on("click",onBtnBackClick)
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      getGCustomerId();
      callApiGetCustomerById();
      callApiGetPaymentsByCustomerId();
      loadDataToTable();
    }
    // Hàm xử lý khi click btn create new payment
    function onBtnCreatePaymentClick() {
      var vPaymentObj = {
        customer: {
          id: gCustomerId
        },
        checkNumber: "",
        paymentDate: "",
        ammount: 0,
      };
      getDataFromInputCreate(vPaymentObj);
      // console.log(vPaymentObj);
      var vIsCheck = validateData(vPaymentObj);
      if (vIsCheck == true) {
        console.log(vPaymentObj);
        callApiPostNewPayment(vPaymentObj);
      }
    }
    // Hàm xử lý khi click btn update payment
    function onBtnUpdatePaymentClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTablePayment.row(vSelectedRow).data();
      console.log(vDataRow);
      gPaymentId = vDataRow.id;
      $('#modal-update-payment').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
      var vPaymentObj = {
        customer: {
          id: gCustomerId
        },
        checkNumber: "",
        paymentDate: "",
        ammount: 0,
      };;
      getDataFromInputUpdate(vPaymentObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vPaymentObj);
      if (vIsCheck == true) {
        callApiPutPaymentById(vPaymentObj);
        $('#modal-update-payment').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete payment
    function onBtnDeletePaymentClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTablePayment.row(vSelectedRow).data();
      console.log(vDataRow);
      gPaymentId = vDataRow.id;
      $('#modal-delete-payment').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeletePayment();
      $('#modal-delete-payment').modal("hide");
    }

    function onBtnBackClick(){
      "use strict"
      window.location.href="customer.html";
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    function getGCustomerId() {
      var vHref = new URL(window.location);
      gCustomerId = vHref.searchParams.get("id");
      console.log("Customer Id = " + gCustomerId);
      if (gCustomerId == null) {
        alert("Không có Customer Id!");
        onBtnBackClick();
      }
    }
    // Hàm call api get all
    function callApiGetPaymentsByCustomerId() {
      $.ajax({
        url: "http://localhost:8080/payments/customerId/" + gCustomerId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayPayment = res;
          console.log(gArrayPayment);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTablePayment.clear();
      gTablePayment.rows.add(gArrayPayment);
      gTablePayment.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
      var today = new Date().toISOString();
      paramObj.checkNumber = $('#inp-checkNumber').val().trim();
      paramObj.paymentDate = today;
      $('#inp-paymentDate').val(today);
      paramObj.ammount = $('#inp-ammount').val();
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      var vResult = false;
      if (paramObj.customer.id == "") {
        alert("Chưa chọn Customer!");
      }
      else if (paramObj.checkNumber == "") {
        alert("Chưa nhập CheckNumber!");
      }
      else if (paramObj.ammount == "") {
        alert("Chưa nhập Ammount!");
      }
      else if (paramObj.ammount < 0 || isNaN(paramObj.ammount)) {
        alert("Ammount phải là số dương!");
      }
      else vResult = true;
      return vResult;
    }
    // Hàm call api post
    function callApiPostNewPayment(paramObj) {
      $.ajax({
        url: gUrl + "/customerId/" + gCustomerId,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetPaymentsByCustomerId();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
      $('#inp-checkNumber').val("");
      $('#inp-paymentDate').val("");
      $('#inp-ammount').val("");
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-paymentId').val(gPaymentId);
      $('#inp-modal-checkNumber').val(paramData.checkNumber);
      $('#inp-modal-paymentDate').val(paramData.paymentDate);
      $('#inp-modal-ammount').val(paramData.ammount);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
      paramObj.checkNumber = $('#inp-modal-checkNumber').val().trim();
      paramObj.paymentDate = $('#inp-modal-paymentDate').val();
      paramObj.ammount = $('#inp-modal-ammount').val();
    }
    // Hàm call api put
    function callApiPutPaymentById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gPaymentId,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetPaymentsByCustomerId();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeletePayment() {
      $.ajax({
        url: gUrl + "/" + gPaymentId,
        type: "DELETE",
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetPaymentsByCustomerId();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm get api lấy tt customer
    function callApiGetCustomerById() {
      $.ajax({
        url: "http://localhost:8080/customers/" + gCustomerId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gCustomerName = res.firstName + " " + res.lastName;
          console.log(res);
          console.log(gCustomerName);
          loadDataToSelectCustomer();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load select customer
    function loadDataToSelectCustomer() {
      $('#select-customer').append($('<option>').val(gCustomerId).html(gCustomerName)).val(gCustomerId);
      $('#select-modal-customer').append($('<option>').val(gCustomerId).html(gCustomerName)).val(gCustomerId);
    }
  });