$(document).ready(function () {
    // Vùng 1: Khai báo biến global
    var gId;
    var gArrayPayment;
    var gArrayCustomer;
    const gCOL_NAME = ["id", "checkNumber", "paymentDate", "ammount", "customerFullName", "action"];
    const gCOL_ID = 0;
    const gCHECK_NUM_COL = 1;
    const gPAYMENT_DATE_COL = 2;
    const gAMMOUNT_COL = 3;
    const gCUSTOMER_COL = 4;
    const gACTION_COL = 5;


    var table = $("#payments-table").DataTable({
        "columns": [
            { "data": gCOL_NAME[gCOL_ID] },
            { "data": gCOL_NAME[gCHECK_NUM_COL] },
            { "data": gCOL_NAME[gPAYMENT_DATE_COL] },
            { "data": gCOL_NAME[gAMMOUNT_COL] },
            { "data": gCOL_NAME[gCUSTOMER_COL] },
            { "data": gCOL_NAME[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
                                <i class="fas fa-trash text-danger" id="btn-delete"></i>`
            }
        ]
    })

    // Vùng 2: Vùng gán
    onPageLoading();
    $("#create-payment").on('click', function () {
        onBtnCreateClick();
    });

    $("#payments-table").on("click", "#btn-update", function () {
        $('#modal-update-payment').modal("show");
        onBtnUpdateTableClick(this);
    })

    $("#update-payment").on("click", function () {
        onBtnUpdateModalClick();
    })


    $('#payments-table').on("click", "#btn-delete", function () {
        $('#modal-delete-payment').modal("show");
        onBtnDeleteTableClick(this);
    })

    $('#delete-payment').on('click', function () {
        onBtnDeleteDataClick();
    })



    // Vùng 3: Khai báo hàm xử lý sự kiện
    function onPageLoading() {
        getAllCustomer();
        getAllPayment();
        loadDataToTable();
    }

    function onBtnCreateClick() {
        var vPaymentObj = {
            customer: {
                id: 0
            },
            checkNumber: "",
            paymentDate: "",
            ammount: 0,
        };
        getDataPayment(vPaymentObj);
        console.log(vPaymentObj);
        var vCheck = validateDataPayment(vPaymentObj);
        if (vCheck) {
            callApiCreatePayment(vPaymentObj);
        }
    }

    function onBtnUpdateTableClick(paramEle){
        "use strict"
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
        $("#inp-modal-paymentId").val(gId);
        $("#inp-modal-checkNumber").val(vDataRow.checkNumber);
        $("#inp-modal-paymentDate").val(vDataRow.paymentDate);
        $("#inp-modal-ammount").val(vDataRow.ammount);
        $("#select-modal-customer").val(vDataRow.customerId);
    }

    function onBtnUpdateModalClick(){
        "use strict";
        var vPaymentObj = {
            customer: {
                id: ""
            },
            checkNumber: "",
            paymentDate: "",
            ammount: 0,
        };
        getDataUpdatePayment(vPaymentObj);
        console.log(vPaymentObj);
        var vCheck = validateDataPayment(vPaymentObj);
        if (vCheck) {
            callApiUpdatePayment(vPaymentObj);
            $("#modal-update-payment").modal("hide");
        }

    }
    function onBtnDeleteTableClick(paramEle) {
        "use strict";
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
    }

    // Vùng 4: Khai báo hàm dùng chung
    function getDataPayment(paramObj) {
        "use strict"
        var today = new Date().toISOString();
        paramObj.checkNumber = $("#inp-checkNumber").val().trim();
        paramObj.paymentDate = today;
        $('#inp-paymentDate').val(today);
        paramObj.ammount = $('#inp-ammount').val().trim();
        paramObj.customer.id = $('#select-customer').val();
    }

    function getDataUpdatePayment(paramObj){
        "use strict"
        var today = new Date().toISOString();
        paramObj.checkNumber = $("#inp-modal-checkNumber").val().trim();
        paramObj.paymentDate = today;
        $('#inp-modal-paymentDate').val(today);
        paramObj.ammount = $('#inp-modal-ammount').val();
        paramObj.customer.id = $('#select-modal-customer').val();
    }

    function validateDataPayment(paramData) {
        "use strict"        
        var vResult = false;
        if (paramData.customer.id == "") {
            alert("Chưa chọn Customer!");
        }
        else if (paramObj.checkNumber == "") {
            alert("Chưa nhập CheckNumber!");
        }
        else if (paramObj.ammount == "") {
            alert("Chưa nhập Ammount!");
        }
        else if (paramObj.ammount < 0 || isNaN(paramObj.ammount)) {
            alert("Ammount phải là số dương!");
        }
        else vResult = true;
        return vResult;
    }

    function callApiCreatePayment(paramDataObj) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/payments/customerId/" + paramDataObj.customer.id,
            data: JSON.stringify(paramDataObj),
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                getAllPayment();
                loadDataToTable();
                clearInpCreate();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function callApiUpdatePayment(paramDataObj){
        "use strict"
        $.ajax({
            url: "http://localhost:8080/payments/" + gId,
            data: JSON.stringify(paramDataObj),
            type: "PUT",
            dataType: "json",
            contentType: 'application/json',
            async: false,
            success: function (res) {
                getAllPayment();
                loadDataToTable();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
    }

    function onBtnDeleteDataClick() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/payments" + "/" + gId,
            type: "DELETE",
            success: function (res) {
                $('#modal-delete-payment').modal('hide');
                getAllPayment();
                loadDataToTable();
            },
            error: function (err) {
                console.log(err.status);
            }
        })
    }

    function clearInpCreate() {
        "use strict"
        $('#inp-checkNumber').val("");
        $('#inp-paymentDate').val("");
        $('#inp-ammount').val("");
        $('#select-customer').val("");
    }

    function getAllCustomer() {
        $.ajax({
            url: "http://localhost:8080/customers",
            type: "GET",
            dataType: "json",
            // async: false,
            success: function (res) {
                console.log(res);
                gArrayCustomer = res;
                loadDataCustomerToSelect();
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function getAllPayment() {
        $.ajax({
            url: "http://localhost:8080/payments",
            type: "GET",
            dataType: "json",
            async: false,
            success: function (res) {
                console.log(res);
                gArrayPayment = res;
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function loadDataToTable() {
        table.clear();
        table.rows.add(gArrayPayment);
        table.draw();
    }

    function loadDataCustomerToSelect() {
        "use strict";
        for (var i = 0; i < gArrayCustomer.length; i++) {
            $('#select-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
            $('#select-modal-customer').append('<option value="' + gArrayCustomer[i].id + '">' + gArrayCustomer[i].firstName + " " + gArrayCustomer[i].lastName + '</option>');
        }
    }
})