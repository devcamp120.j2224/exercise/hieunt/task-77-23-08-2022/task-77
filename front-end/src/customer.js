$(document).ready(function () {
    //Vùng 1
    var gId = 0;
    var gCustomerId;
    const gCOL_NAME = ["id", "firstName", "lastName", "phoneNumber", "address", "city", "country", "creditLimit", "action"];
    const gCOL_ID = 0;
    const gFIRST_NAME = 1;
    const gLAST_NAME = 2;
    const gPHONE_NUMBER = 3;
    const gADDRESS = 4;
    const gCITY = 5;
    const gCOUNTRY = 6;
    const gCREDIT_LIMIT = 7;
    const gACTION = 8;

    var table = $("#customer-table").DataTable({
        "columns": [
            { "data": gCOL_NAME[gCOL_ID] },
            { "data": gCOL_NAME[gFIRST_NAME] },
            { "data": gCOL_NAME[gLAST_NAME] },
            { "data": gCOL_NAME[gPHONE_NUMBER] },
            { "data": gCOL_NAME[gADDRESS] },
            { "data": gCOL_NAME[gCITY] },
            { "data": gCOL_NAME[gCOUNTRY] },
            { "data": gCOL_NAME[gCREDIT_LIMIT] },
            { "data": gCOL_NAME[gACTION] },
        ],
        columnDefs: [
            {
                targets: gACTION,
                defaultContent: `
                    <i class="far fa-plus-square" id="create-customer"></i>
                    | <i class="far fa-edit" id="update-customer"></i>
                    | <i class="far fa-trash-alt" id="delete-customer"></i>
                    | <i class="fas fa-info" id="detail-customer-order"></i>
                    | <i class="fas fa-info-circle" id="detail-customer-payment"></i>`
            }
        ]
    })

    //
    onPageLoading();
    $("#customer-table").on('click', '#create-customer', function () {
        $("#customer-modal").modal("show");
    });

    $('#btn-create-customer').on('click', function () {
        onBtnCreateModalClick();
    })

    $("#customer-table").on("click", "#update-customer", function () {
        $('#customer-modal-update').modal("show");
        onBtnUpdateTableClick(this);
    })

    $("#btn-update-customer").on("click", function () {
        onBtnUpdateModalClick();
    })


    $('#customer-table').on("click", "#delete-customer", function () {
        $('#modal-delete-customer').modal("show");
        onBtnDeleteTableClick(this);
    })

    $('#delete-modal-customer').on('click', function () {
        onBtnDeleteDataClick();
    })

    $("#customer-table").on("click","#detail-customer-order",function(){
        onBtnDetailCustomerOrder(this);
    })
    $("#customer-table").on("click","#detail-customer-payment",function(){
        onBtnDetailCustomerPayment(this);
    })



    //3
    function onPageLoading() {
        getAllCustomer();
    }

    function getAllCustomer() {
        $.ajax({
            url: "http://localhost:8080/customers",
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res);
            },
            error: function (err) {
                console.log(err.response);
            }
        })
    }

    function onBtnCreateModalClick() {
        var vData = {
            firstName: "",
            lastName: "",
            phoneNumber: "",
            address: "",
            city: "",
            country: "",
            creditLimit: ""
        }

        vData.firstName = $("#input-modal-first-name").val().trim();
        vData.lastName = $("#input-modal-last-name").val().trim();
        vData.phoneNumber = $("#input-modal-phone-number").val().trim();
        vData.address = $("#input-modal-address").val().trim();
        vData.city = $("#input-modal-city").val().trim();
        vData.country = $("#input-modal-country").val().trim();
        vData.creditLimit = $("#input-modal-credit-limit").val().trim();

        var vCheck = validateData(vData);
        if (vCheck) {
            callApiCreateCustomer(vData);
        }
    }

    function onBtnDetailCustomerOrder(paramEle){
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gCustomerId = vDataRow.id;
        window.location.href = "customerOrder.html?id=" + gCustomerId;
    }

    function onBtnDetailCustomerPayment(paramEle){
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gCustomerId = vDataRow.id;
        window.location.href = "customerPayment.html?id=" + gCustomerId;
    }
    //4
    function validateData(paramData) {
        "use strict"
        var vIsValid = false;
        if (paramData.firstName == "") {
            alert("First name không được để trống!");
        }
        else if (paramData.lastName == "") {
            alert("Last name không được để trống!");
        }
        else if (paramData.phoneNumber == "") {
            alert("Phone number không được để trống!");
        }
        else if (isNaN(paramData.phoneNumber)) {
            alert("Vui lòng nhập Phone number là số!");
        }
        else if (paramData.address == "") {
            alert("Address không được để trống!");
        }
        else if (paramData.city == "") {
            alert("City không được để trống!");
        }
        else if (paramData.country == "") {
            alert("Country không được để trống!");
        }
        else if (paramData.creditLimit == "") {
            alert("Credit Limit không được để trống!");
        }
        else if (paramData.creditLimit <=0 || Number.isInteger(Number(paramData.creditLimit)) == false) {
            alert("Credit Limit phải là số nguyên dương!");
        }
        else vIsValid = true;
        return vIsValid;
    }

    function callApiCreateCustomer(paramData) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/customers",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(paramData),
            success: function (res) {
                alert("Tạo thành công customer");
                $("#customer-modal").modal("hide");
                getAllCustomer();
                console.log(res);
            },
            error: function (err) {
                console.log(err.status);
            }
        })
    }

    function loadDataToTable(paramData) {
        table.clear();
        table.rows.add(paramData);
        table.draw();
    }

    function onBtnUpdateTableClick(paramEle) {
        var vTableRow = $(paramEle).closest("tr");
        var vDataRow = table.row(vTableRow).data();

        gId = vDataRow.id;
        console.log(vDataRow)
        $("#input-modal-update-first-name").val(vDataRow.firstName);
        $("#input-modal-update-last-name").val(vDataRow.lastName);
        $("#input-modal-update-phone-number").val(vDataRow.phoneNumber);
        $("#input-modal-update-address").val(vDataRow.address);
        $("#input-modal-update-city").val(vDataRow.city);
        $("#input-modal-update-country").val(vDataRow.country);
        $("#input-modal-update-credit-limit").val(vDataRow.creditLimit);
    }

    function onBtnUpdateModalClick() {
        var vData = {
            firstName: "",
            lastName: "",
            phoneNumber: "",
            address: "",
            city: "",
            country: "",
            creditLimit: ""
        }

        vData.firstName = $("#input-modal-update-first-name").val().trim();
        vData.lastName = $("#input-modal-update-last-name").val().trim();
        vData.phoneNumber = $("#input-modal-update-phone-number").val().trim();
        vData.address = $("#input-modal-update-address").val().trim();
        vData.city = $("#input-modal-update-city").val().trim();
        vData.country = $("#input-modal-update-country").val().trim();
        vData.creditLimit = $("#input-modal-update-credit-limit").val().trim();

        var vCheck = validateData(vData);
        if (vCheck) {
            callApiUpdateCustomer(vData);
        }

        function callApiUpdateCustomer(paramData) {
            "use strict";
            $.ajax({
                url: "http://localhost:8080/customers" + "/" + gId,
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify(paramData),
                success: function (res) {
                    alert("Cập nhật thành công customer");
                    $("#customer-modal-update").modal("hide");
                    getAllCustomer();
                    console.log(res);
                },
                error: function (err) {
                    console.log(err.status);
                }
            })
        }
    }
    function onBtnDeleteTableClick(paramEle) {
        "use strict";
        var vTable = $(paramEle).closest("tr");
        var vDataRow = table.row(vTable).data();
        gId = vDataRow.id;
        console.log(gId)
    }

    function onBtnDeleteDataClick() {
        "use strict";
        $.ajax({
            url: "http://localhost:8080/customers" + "/" + gId,
            type: "DELETE",
            success: function (res) {
                alert("Xóa thành công customer");
                $('#modal-delete-customer').modal('hide');
                getAllCustomer();
                console.log(res);
            },
            error: function (err) {
                console.log(err.status);
            }
        })
    }
})