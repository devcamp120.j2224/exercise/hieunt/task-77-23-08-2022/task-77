$(document).ready(function() {
    // Vùng 1: Khai báo biến global
    var gUrl = "http://localhost:8080/orderdetails";
    var gOrderDetailId = 0;
    var gOrderId;
    var gArrayOrderDetail;
    var gArrayProduct;
    const gID_COL = 0;
    const gORDER_ID_COL = 1;
    const gPRODUCT_NAME_COL = 2;
    const gQUANTITY_ORDER_COL = 3;
    const gPRICE_EACH_COL = 4;
    const gACTION_COL = 5
    var gNameCol = ["id", "orderId", "productName", "quantityOrder", "priceEach", "action"];
    var gTableOrderDetail = $('#orderDetail-table').DataTable({
      columns: [
        { data: gNameCol[gID_COL] },
        { data: gNameCol[gORDER_ID_COL] },
        { data: gNameCol[gPRODUCT_NAME_COL] },
        { data: gNameCol[gQUANTITY_ORDER_COL] },
        { data: gNameCol[gPRICE_EACH_COL] },
        { data: gNameCol[gACTION_COL] }
      ],
      columnDefs: [
        {
          targets: gACTION_COL,
          defaultContent: `<i class="fas fa-edit text-primary" id="btn-update"></i>
          | <i class="fas fa-trash text-danger" id="btn-delete"></i>`
        }
      ]
    });
  
    // Vùng 2: Vùng gán
    onPageLoading();
    $('#create-orderDetail').on('click', onBtnCreateOrderDetailClick);
    $('#orderDetail-table').on('click', "#btn-update", function() {
      onBtnUpdateOrderDetailClick(this);
    });
    $('#update-orderDetail').on('click', onBtnUpdateModalClick);
    $('#orderDetail-table').on('click', "#btn-delete", function() {
      onBtnDeleteOrderDetailClick(this);
    });
    $('#delete-orderDetail').on('click', onBtnDeleteConfirmClick);

    $("#back-order").on("click",onBtnBackClick)
  
    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
      getGOrderId();
      loadDataToSelectOrder();
      callApiGetAllProduct();
      callApiGetOrderDetailsByOrderId();
      loadDataToTable();
    }
    // Hàm xử lý khi click btn create new orderDetail
    function onBtnCreateOrderDetailClick() {
      var vOrderDetailObj = {
        order: {
          id: gOrderId
        },
        product: {
          id: ""
        },
        quantityOrder: 0,
        priceEach: 0
      };
      getDataFromInputCreate(vOrderDetailObj);
      // console.log(vOrderObj);
      var vIsCheck = validateData(vOrderDetailObj);
      if (vIsCheck == true) {
        console.log(vOrderDetailObj);
        callApiPostNewOrderDetail(vOrderDetailObj);
      }
    }
    // Hàm xử lý khi click btn update orderDetail
    function onBtnUpdateOrderDetailClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrderDetail.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderDetailId = vDataRow.id;
      $('#modal-update-orderDetail').modal("show");
      loadDataToModalUpdate(vDataRow);
    }
    // Hàm xử lý khi click btn update in modal
    function onBtnUpdateModalClick() {
      var vOrderDetailObj = {
        order: {
          id: gOrderId
        },
        product: {
          id: ""
        },
        quantityOrder: 0,
        priceEach: 0
      };
      getDataFromInputUpdate(vOrderDetailObj);
      // console.log(gCustomerId);
      // console.log(vCustomerObj);
      var vIsCheck = validateData(vOrderDetailObj);
      if (vIsCheck == true) {
        callApiPutOrderDetailById(vOrderDetailObj);
        $('#modal-update-orderDetail').modal("hide");
      }
    }
    // Hàm xử lý khi click btn delete orderDetail
    function onBtnDeleteOrderDetailClick(paramEle) {
      var vSelectedRow = $(paramEle).parents("tr");
      var vDataRow = gTableOrderDetail.row(vSelectedRow).data();
      console.log(vDataRow);
      gOrderDetailId = vDataRow.id;
      $('#modal-delete-orderDetail').modal("show");
    }
    // Hàm xử lý khi click confirm delete
    function onBtnDeleteConfirmClick() {
      callApiDeleteOrderDetail();
      $('#modal-delete-orderDetail').modal("hide");
    }

    function onBtnBackClick(){
      "use strict"
      window.location.href="order.html";
    }
  
    // Vùng 4: Khai báo hàm dùng chung
    function getGOrderId() {
      var vHref = new URL(window.location);
      gOrderId = vHref.searchParams.get("id");
      console.log("Order Id = " + gOrderId);
      if (gOrderId == null) {
        alert("Không có Order Id!");
        onBtnBackClick();
      }
    }
    // Hàm call api get all
    function callApiGetOrderDetailsByOrderId() {
      $.ajax({
        url: gUrl + "/orderId/" + gOrderId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(res) {
          gArrayOrderDetail = res;
          console.log(res);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load data to table
    function loadDataToTable() {
      gTableOrderDetail.clear();
      gTableOrderDetail.rows.add(gArrayOrderDetail);
      gTableOrderDetail.draw();
    }
    // Hàm get data from input create
    function getDataFromInputCreate(paramObj) {
      paramObj.quantityOrder = $('#inp-quantityOrder').val();
      paramObj.priceEach = $('#inp-priceEach').val();
      paramObj.order.id = $('#select-order').find(":selected").val();
      paramObj.product.id = $('#select-product').find(":selected").val();
    }
    // Hàm validate dữ liệu
    function validateData(paramObj) {
      var vIsValid = false;
      if (paramObj.order.id == "") {
        alert("Chưa chọn Order!");
      }
      else if (paramObj.product.id == "") {
        alert("Phải chọn product!");
      }
      else if (paramObj.quantityOrder == "") {
        alert("Phải nhập quantityOrder!");
      }
      else if (paramObj.quantityOrder <= 0 || Number.isInteger(Number(paramObj.quantityOrder)) == false) {
        alert("quantityOrder phải là số nguyên dương!");
      }
      else if (paramObj.priceEach == "") {
        alert("Phải nhập priceEach!");
      }
      else if (paramObj.priceEach <= 0) {
        alert("priceEach phải là số dương!");
      }
      else vIsValid = true;
      return vIsValid;
    }
    // Hàm call api post
    function callApiPostNewOrderDetail(paramObj) {
      $.ajax({
        url: gUrl + "/orderId/" + gOrderId + "/productId/" + paramObj.product.id,
        data: JSON.stringify(paramObj),
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetOrderDetailsByOrderId();
          loadDataToTable();
          clearInpCreate();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm clear inp create
    function clearInpCreate() {
      $('#inp-quantityOrder').val("");
      $('#inp-priceEach').val("");
      $('#select-product').val("0").prop('selected', true);
    }
    // Hàm load data to modal update
    function loadDataToModalUpdate(paramData) {
      $('#inp-modal-orderDetailId').val(gOrderDetailId)
      $('#inp-modal-quantityOrder').val(paramData.quantityOrder);
      $('#inp-modal-priceEach').val(paramData.priceEach);
      $('#select-modal-product').val(paramData.productId);
    }
    // Hàm get data from input update
    function getDataFromInputUpdate(paramObj) {
      paramObj.quantityOrder = $('#inp-modal-quantityOrder').val();
      paramObj.priceEach = $('#inp-modal-priceEach').val();
      paramObj.product.id = $('#select-modal-product').find(":selected").val();
    }
    // Hàm call api put
    function callApiPutOrderDetailById(paramObj) {
      $.ajax({
        url: gUrl + "/" + gOrderDetailId + "/productId/" + paramObj.product.id,
        data: JSON.stringify(paramObj),
        type: "PUT",
        contentType: 'application/json',
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetOrderDetailsByOrderId();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm call api delete
    function callApiDeleteOrderDetail() {
      $.ajax({
        url: gUrl + "/" + gOrderDetailId,
        type: "DELETE",
        async: false,
        success: function(res) {
          console.log(res);
          callApiGetOrderDetailsByOrderId();
          loadDataToTable();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    
    // Hàm call api get all product
    function callApiGetAllProduct() {
      $.ajax({
        url: "http://localhost:8080/products",
        type: "GET",
        async: false,
        dataType: "json",
        success: function(res) {
          console.log(res);
          gArrayProduct = res;
          loadDataToSelectProduct();
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    }
    // Hàm load select product
    function loadDataToSelectProduct() {
      for (var i = 0; i < gArrayProduct.length; i++) {
        $('#select-product').append('<option value="' + gArrayProduct[i].id + '">' + gArrayProduct[i].productName + '</option>');
        $('#select-modal-product').append('<option value="' + gArrayProduct[i].id + '">' + gArrayProduct[i].productName + '</option>');
      }
    }
    // Hàm load select order
    function loadDataToSelectOrder() {
      $('#select-order').append('<option value="' + gOrderId + '">' + gOrderId + '</option>').val(gOrderId);
      $('#select-modal-order').append('<option value="' + gOrderId + '">' + gOrderId + '</option>').val(gOrderId);
    }
  });